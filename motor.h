/*
 * motor.h
 *
 *  Created on: 2013/06/14
 *      Author: yuichi
 */

#ifndef MOTOR_H_
#define MOTOR_H_

class Motor{
private:
	TIM_TypeDef *TIM_Enc;
	__IO uint32_t *CCR_F;
	__IO uint32_t *CCR_B;
	uint16_t prvX;
	uint16_t nowX;
	float prvE;
	float nowE;
	float u;
	int prvV;
	int nowV;
	int milliMeterPerSec;
	long long V_i;
	long long V_d;
	long long V;
	float K_p;
	float K_i;
	float K_d;
	uint8_t PWM_Offset;
public:
	Motor(TIM_TypeDef *TIM_E,__IO uint32_t *CCR_F,__IO uint32_t *CCR_B):
		TIM_Enc(TIM_E),
		CCR_F(CCR_F),
		CCR_B(CCR_B),
		prvX(TIM_E->CNT),
		nowX(prvX),
		prvE(0),
		nowE(0),
		u(0),
		prvV(0),
		nowV(0),
		milliMeterPerSec(0),
		V_i(0),V_d(0),V(0),
		K_p(0.3f),K_i(0.f),K_d(0.4f),
		PWM_Offset(0){}
	Motor(){}
	~Motor(){}
	void setMilliMeterPerSec(int val);
	void MotorService();
	void setMotorPID(float _K_p,float _K_i,float _K_d);
	void setMotorOffset(uint8_t offset){ PWM_Offset=offset; }
	int getV() const{ return nowV; }
	void eStop();
	void initialize();
};

#endif /* MOTOR_H_ */
