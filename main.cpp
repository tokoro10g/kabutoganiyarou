#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "adc.h"
#include "button.h"
#include "buzzer.h"
#include "cmd.h"
#include "graph.hpp"
#include "hdsp2000lp.h"
#include "i2c.h"
#include "maze.hpp"
#include "mouse.hpp"
#include "mymath.h"
#include "service.h"
#include "usart.h"
#include "viewer.hpp"
#include "machine.h"
#include "eeptools.h"
#include "userfunctors.h"

#include "Logger.h"

#include <vector>

using namespace std;

bool AutoFlag=false;
bool FailFlag=false;
uint8_t runcount=0;

uint32_t Count = 0x1FFFF;
Mouse *mouse;

extern uint8_t *USART_RxBuffer;
extern uint8_t *USART_TxBuffer;
extern SrvStat_t ServiceStatus;

void GPIO_Configuration(void);
void Delay(__IO uint32_t nCount);

int main(void) {
	SystemInit();
	SystemCoreClockUpdate();

	DBGMCU_Config(DBGMCU_STOP,ENABLE);

	GPIO_PinAFConfig(GPIOA,GPIO_PinSource13,GPIO_AF_SWJ);
	GPIO_PinAFConfig(GPIOA,GPIO_PinSource14,GPIO_AF_SWJ);

	GPIO_InitTypeDef GPIO_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	RCC_ClocksTypeDef RCC_ClockFreq;

	RCC_GetClocksFreq(&RCC_ClockFreq);
	RCC_ClockSecuritySystemCmd(ENABLE);

	RCC_AHB1PeriphClockCmd(
			RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_DMA1 | RCC_AHB1Periph_DMA2, ENABLE);
	RCC_APB1PeriphClockCmd(
			RCC_APB1Periph_TIM12 | RCC_APB1Periph_TIM3 | RCC_APB1Periph_TIM4 | RCC_APB1Periph_TIM5, ENABLE);
	RCC_APB2PeriphClockCmd(
			RCC_APB2Periph_ADC1 | RCC_APB2Periph_ADC2 | RCC_APB2Periph_TIM10 | RCC_APB2Periph_TIM9 | RCC_APB2Periph_TIM8, ENABLE);

	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	FLASH_Unlock();
	EEP_Init();
	Machine::getInstance().initialize();
/*
	EEP_WriteFloat(PARAM_MOTOR_P,1.f);
	EEP_WriteFloat(PARAM_MOTOR_I,0.5f);
	EEP_WriteFloat(PARAM_MOTOR_D,2.f);

	EEP_WriteFloat(PARAM_POS_P,18.f);
	EEP_WriteFloat(PARAM_POS_I,1.2f);
	EEP_WriteFloat(PARAM_POS_D,90.f);

	EEP_WriteFloat(PARAM_ANG_P,17.f);
	EEP_WriteFloat(PARAM_ANG_I,1.2f);
	EEP_WriteFloat(PARAM_ANG_D,80.f);

	EEP_Write(PARAM_MOTOR_OFFSET,0);

	EEP_WriteFloat(PARAM_RANG_P,2.f);
	EEP_WriteFloat(PARAM_RANG_D,5.f);

	EEP_Write(PARAM_WALL_FRONT_THRESHOLD,500);
	EEP_Write(PARAM_WALL_SIDE_THRESHOLD,800);
	EEP_Write(PARAM_OFFSET_ANGLE_ONE_WALL,512);
	EEP_Write(PARAM_OFFSET_ANGLE_TWO_WALLS,512);
	EEP_Write(PARAM_OFFSET_COORD_ONE_WALL,512);
	EEP_Write(PARAM_OFFSET_COORD_TWO_WALLS,2048);
*/
	float K_p,K_i,K_d;
	EEP_ReadFloat(PARAM_MOTOR_P,&K_p);
	EEP_ReadFloat(PARAM_MOTOR_I,&K_i);
	EEP_ReadFloat(PARAM_MOTOR_D,&K_d);
	Machine::getInstance().setMotorPID(K_p,K_i,K_d);
	EEP_ReadFloat(PARAM_POS_P,&K_p);
	EEP_ReadFloat(PARAM_POS_I,&K_i);
	EEP_ReadFloat(PARAM_POS_D,&K_d);
	Machine::getInstance().setMachinePID(K_p,K_i,K_d);
	EEP_ReadFloat(PARAM_ANG_P,&K_p);
	EEP_ReadFloat(PARAM_ANG_I,&K_i);
	EEP_ReadFloat(PARAM_ANG_D,&K_d);
	Machine::getInstance().setAnglePID(K_p,K_i,K_d);
	EEP_ReadFloat(PARAM_RANG_P,&K_p);
	EEP_ReadFloat(PARAM_RANG_D,&K_d);
	Machine::getInstance().setRAnglePD(K_p,K_d);
	uint16_t opt;
	EEP_Read(PARAM_SRV_OPT,&opt);
	ServiceStatus.srvOpt=opt;
	EEP_Read(PARAM_MOTOR_OFFSET,&opt);
	Machine::getInstance().setMotorPWMOffset(opt&0xFF);
	uint16_t Th_f,Th_s,Offs_a1,Offs_a2,Offs_c1,Offs_c2;
	EEP_Read(PARAM_WALL_FRONT_THRESHOLD,&Th_f);
	EEP_Read(PARAM_WALL_SIDE_THRESHOLD,&Th_s);
	EEP_Read(PARAM_OFFSET_ANGLE_ONE_WALL,&Offs_a1);
	EEP_Read(PARAM_OFFSET_ANGLE_TWO_WALLS,&Offs_a2);
	EEP_Read(PARAM_OFFSET_COORD_ONE_WALL,&Offs_c1);
	EEP_Read(PARAM_OFFSET_COORD_TWO_WALLS,&Offs_c2);
	Machine::getInstance().setOffsetConst(Th_f,Th_s,Offs_a1,Offs_a2,Offs_c1,Offs_c2);



	USART_Config();
	USART_printf("Clock: %d Hz\r\n",1,SystemCoreClock);
	USART_Puts("USART Configured.\r\n");

	NVIC_InitStruct.NVIC_IRQChannel = RCC_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = TIM8_BRK_TIM12_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = TIM1_BRK_TIM9_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = I2C2_ER_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = TIM5_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 2;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	USART_Puts("NVIC Configured.\r\n");

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_8 | GPIO_Pin_9 | GPIO_Pin_10
			|GPIO_Pin_11 | GPIO_Pin_12;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* ADC123_CH[0-3] */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_2
			| GPIO_Pin_3;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOA, &GPIO_InitStruct);

	/* IR-LED */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStruct);

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5 | GPIO_Pin_6
			|GPIO_Pin_7;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource4, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource5, GPIO_AF_TIM3);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource6, GPIO_AF_TIM4);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource7, GPIO_AF_TIM4);

	USART_Puts("GPIO Configured.\r\n");

	HDSP_SetStatus(HDSP_STATUS_ENABLE);
	HDSP_LoadStr("    ", HDSP_ASCII);
	HDSP_SetStatus(ServiceStatus.srvOpt&1);

	USART_Puts("HDSP Configured.\r\n");

	ADC_Config();
	USART_Puts("ADC Configured.\r\n");

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_Period = 1000;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStruct.TIM_Prescaler = (uint16_t)(
			(SystemCoreClock) / 1000000) - 1; // 1MHz
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM10, &TIM_TimeBaseInitStruct);
	TIM_ITConfig(TIM10, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM10, ENABLE);

	TIM_TimeBaseInitStruct.TIM_Period = 5000;
	TIM_TimeBaseInitStruct.TIM_ClockDivision=0;
	TIM_TimeBaseInitStruct.TIM_Prescaler=(uint16_t)((SystemCoreClock/2)/1000000)-1;
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM12, &TIM_TimeBaseInitStruct);

	TIM_TimeBaseInitStruct.TIM_Period = 5000;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStruct.TIM_Prescaler = (uint16_t)(
			(SystemCoreClock) / 1000000) - 1; // 1MHz
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM9, &TIM_TimeBaseInitStruct);

	TIM_TimeBaseInitStruct.TIM_Period = 16;
	TIM_TimeBaseInitStruct.TIM_ClockDivision=0;
	TIM_TimeBaseInitStruct.TIM_Prescaler=(uint16_t)((SystemCoreClock/2)/1000)-1;
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM5,&TIM_TimeBaseInitStruct);
	TIM_ITConfig(TIM5,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM5,ENABLE);

	USART_Puts("Timer Configured.\r\n");

	/* Encoder */
	TIM_TimeBaseStructInit(&TIM_TimeBaseInitStruct);
	TIM_TimeBaseInitStruct.TIM_Period = 0xffffffff;
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Down;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStruct);
	TIM_TimeBaseInitStruct.TIM_CounterMode=TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM4, &TIM_TimeBaseInitStruct);
	TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12,
			TIM_ICPolarity_Falling, TIM_ICPolarity_Rising);
	TIM_EncoderInterfaceConfig(TIM4, TIM_EncoderMode_TI12,
			TIM_ICPolarity_Rising, TIM_ICPolarity_Rising);
	TIM_Cmd(TIM3, ENABLE);
	TIM_Cmd(TIM4, ENABLE);

	USART_Puts("Encoder Configured.\r\n");

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_8 | GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource6, GPIO_AF_TIM8);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource7, GPIO_AF_TIM8);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource8, GPIO_AF_TIM8);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource9, GPIO_AF_TIM8);

	TIM_TimeBaseInitStruct.TIM_Period = 1000;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStruct.TIM_Prescaler = (uint16_t)( (SystemCoreClock) / 30000000) - 1; // 1MHz
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM8, &TIM_TimeBaseInitStruct);

	TIM_OCInitTypeDef TIM_OCInitStruct;
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_OutputNState=TIM_OutputNState_Disable;
	TIM_OCInitStruct.TIM_OCIdleState=TIM_OCIdleState_Reset;
	TIM_OCInitStruct.TIM_OCNIdleState=TIM_OCNIdleState_Reset;
	TIM_OCInitStruct.TIM_OCNPolarity=TIM_OCNPolarity_Low;
	TIM_OCInitStruct.TIM_Pulse = 0;
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM8,&TIM_OCInitStruct);
	TIM_OC2Init(TIM8,&TIM_OCInitStruct);
	TIM_OC3Init(TIM8,&TIM_OCInitStruct);
	TIM_OC4Init(TIM8,&TIM_OCInitStruct);
	TIM_OC1PreloadConfig(TIM8, TIM_OCPreload_Enable);
	TIM_OC2PreloadConfig(TIM8, TIM_OCPreload_Enable);
	TIM_OC3PreloadConfig(TIM8, TIM_OCPreload_Enable);
	TIM_OC4PreloadConfig(TIM8, TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM8, ENABLE);
	TIM_Cmd(TIM8, ENABLE);
	TIM_CtrlPWMOutputs(TIM8,ENABLE);

	Button_Config();
	USART_Puts("Button Configured.\r\n");
	Buzzer_Config();
	USART_Puts("Buzzer Configured.\r\n");

	Buzzer_SetStatus((ServiceStatus.srvOpt & 4) >> 2);
	Buzzer_Add(1047, 100);
	Buzzer_Add(0, 50);
	Buzzer_Add(1047, 100);
	Buzzer_Play();

	I2C_Config();

	I2C_GyroCmd(0x20,0x9f);
	I2C_GyroCmd(0x23,0x20);
	I2C_GyroCmd(0x24,0x02);
	I2C_AccCmd(0x20,0xc7);
	USART_Puts("I2C Configured.\r\n");

	USART_Puts("Ready\r\n");
/*
	HDSP_LoadStr("WAIT",HDSP_ASCII);
	Delay(0x388888);
	HDSP_LoadStr("AITI",HDSP_ASCII);
	Delay(0x388888);
	HDSP_LoadStr("ITIN",HDSP_ASCII);
	Delay(0x388888);
	HDSP_LoadStr("TING",HDSP_ASCII);
	Delay(0x388888);
	HDSP_LoadStr("ING.",HDSP_ASCII);
	Delay(0x388888);
	HDSP_LoadStr("NG..",HDSP_ASCII);
	Delay(0x388888);
	HDSP_LoadStr("G...",HDSP_ASCII);
	Delay(0x788888);
	*/

	TIM_ITConfig(TIM12,TIM_IT_Update,ENABLE);
	TIM_Cmd(TIM12,ENABLE);
	TIM_ITConfig(TIM9, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM9, ENABLE);

	Delay(0xFFFF);

	Maze maze(16);
	CellData cell={{0},{0},{0},{0}};
	for(int i=0;i<16;i++){
		for(int j=0;j<16;j++){
			cell.wall.half=0;
			if(j==0&&i==15)
				cell.wall.bits.EAST=1;
			if(j==1&&i==15)
				cell.wall.bits.WEST=1;
			if(i==0)
				cell.wall.bits.NORTH=1;
			if(j==0)
				cell.wall.bits.WEST=1;
			if(j==15)
				cell.wall.bits.EAST=1;
			if(i==15)
				cell.wall.bits.SOUTH=1;
			maze.addCell(cell);
		}
	}
	//uint8_t goal=135;
	uint8_t goal=241;
	//uint8_t goal=192;

    mouse=new Mouse(maze,16);
    //AutoFlag=true;
	while (1) {
		if(AutoFlag==true){
			runcount++;
			Machine::getInstance().activate();
			HDSP_LoadStr("CLBR",HDSP_ASCII);
			TIM_ITConfig(TIM12,TIM_IT_Update,DISABLE);
			Delay(0x788888);
			Machine::getInstance().calibrateSensors();
			Machine::getInstance().initializeVariables();
			Coord c;
			c.x=0;
			c.y=15;
			c.dir.half=1;
			mouse->setCoord(c);
			TIM_ITConfig(TIM12,TIM_IT_Update,ENABLE);
			Delay(0x788888);
			Machine::getInstance().setWallCtrl(true);

			if(FailFlag){
				mouse->clearMaze();
				FailFlag=false;
			}
			Route route;
			if(runcount==1){
				route=mouse->searchMaze(240,goal,true);
			} else {
				route=mouse->searchMaze(240,goal,false);
				Machine::getInstance().setLoggingState(true);
				mouse->generateFastestPath(route);
				while(!Machine::getInstance().isTargetQueueEmpty());
			}
			if(mouse->getCoordIndex()==goal){
				c=mouse->getCoord();
				uint8_t d=c.dir.half;
				int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
				Position p(c.x*180,(15-c.y)*180,angle);
				Machine::getInstance().setDestinationInMilliMeter(p,30,new MotionLinear(new EasingPoly3Out()),true);
				Delay(0x788888);
				Buzzer_PlayEnchukei();
				Machine::getInstance().setLoggingState(false);
				if(runcount==1){
					mouse->searchMaze(goal,0,true);
					if(mouse->getCoordIndex()==0){
						Buzzer_Add(880, 30);
						Buzzer_Add(1760, 30);
						Buzzer_Add(3520, 30);
					} else {
						//FailFlag=true;
						Buzzer_Add(1760, 100);
						Buzzer_Add(880, 100);
						Buzzer_Add(1760, 100);
						Buzzer_Add(880, 100);
					}
				}
			} else {
				FailFlag=true;
				Buzzer_Add(1760, 100);
				Buzzer_Add(880, 100);
				Buzzer_Add(1760, 100);
				Buzzer_Add(880, 100);
				Delay(0x788888);
			}
			mouse->searchMaze(mouse->getCoordIndex(),240,true);
			if(mouse->getCoordIndex()==240){
				c=mouse->getCoord();
				uint8_t d=c.dir.half;
				int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
				Machine::getInstance().setDestinationInMilliMeter(Position(0,20,angle),30,new MotionLinear(new EasingPoly3Out()),true);
				Machine::getInstance().setDestinationInMilliMeter(Position(0,20,0),200,new MotionLinear(new EasingPoly3WithPause()),false);
				Buzzer_Add(880, 30);
				Buzzer_Add(1760, 30);
				Buzzer_Add(3520, 30);
			} else {
				mouse->clearMaze();
				FailFlag=true;
				Buzzer_Add(880, 600);
			}
			AutoFlag=false;
			Machine::getInstance().setWallCtrl(false);
			while(!Machine::getInstance().isTargetQueueEmpty());
			Machine::getInstance().deactivate();
		} else if(GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_0)==SET&&GPIO_ReadInputDataBit(GPIOC,GPIO_Pin_1)==SET){
			Cmd_Proc();
			Delay(0x18888);
		} else {
			Delay(0x18888);
		}
	}
	return 0;
}

void Delay(__IO uint32_t nCount) {
	for (; nCount != 0; nCount--)
		;
}
