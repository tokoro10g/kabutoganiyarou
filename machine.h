/*
 * machine.h
 *
 *  Created on: 2013/06/19
 *      Author: yuichi
 */

#ifndef MACHINE_H_
#define MACHINE_H_

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"
#include "motor.h"
#include "eeptools.h"
#include "Logger.h"

#include "functors.h"
#include "position.h"

#include "mymath.h"

#include <queue>
#include <functional>

class Target {
private:
	Position origin;
	Position dest;
	unsigned int time;
	MotionFunctor *mf;
	bool wallCtrlState;
public:
	Target(Position _origin,Position _dest,unsigned int _time,MotionFunctor *_mf,bool _isw):origin(_origin),dest(_dest),time(_time),mf(_mf),wallCtrlState(_isw){
		mf->setParams(origin,dest,time);
	}
	Target(const Target& t){
		origin=t.origin;
		dest=t.dest;
		time=t.time;
		mf=t.mf;
		mf->setParams(origin,dest,time);
	}
	Position checkOrigin() { return mf->func(0); }
	Position checkDest() { return mf->func(time); }
	Position getCurrentTarget(unsigned int t){ return mf->func(t); }
	Position getDest(){ return dest; }
	MotionFunctor* getMF() { return mf; }
	void destroy(){ mf->destroy(); delete mf; }
	unsigned int getTime() const { return time; }
	bool isWallCtrlActive() const { return wallCtrlState; }
};

class Machine{
private:
	Motor motorL;
	Motor motorR;
	volatile long angle;
	volatile long omega;
	volatile float rangle;
	volatile long x;
	volatile long y;
	volatile long v;
	volatile long e;
	volatile long tangle;
	volatile long tx;
	volatile long ty;
	volatile unsigned int time;
	volatile long a_i;
	volatile long e_i;
	volatile long olda;
	volatile long olde;
	volatile long oldea;
	volatile long oldgzi;
	volatile float oldrangle;
	float Kp_p;
	float Kp_i;
	float Kp_d;
	float Kra_p;
	float Kra_d;
	float Ka_p;
	float Ka_i;
	float Ka_d;
	uint16_t Th_f;
	uint16_t Th_s;
	uint16_t Offs_a1;
	uint16_t Offs_a2;
	uint16_t Offs_c1;
	uint16_t Offs_c2;
	bool activeflag;
	bool wallCtrlFlag;
	std::queue<Target*> targetQueue;
	Position fdest;
	Logger *xLogger;
	Logger *yLogger;
	Logger *angleLogger;
	Logger *txLogger;
	Logger *tyLogger;
	Logger *rangleLogger;
	Logger *eaLogger;
	Logger *aiLogger;
	Logger *rvLogger;
	Logger *romegaLogger;
	bool isLogging;
	int gx,gy,gz;
	int oldgx,oldgy,oldgz;
	int gyroDefVal[3];
	int ax,ay,az;
	int oldax,olday,oldaz;
	int accDefVal[3];
	long adcDefVal[4];
	long gx_i;
	long gy_i;
	long gz_i;
	long ax_i;
	long ay_i;
	long az_i;
private:
	Machine(){};
	Machine(Machine const &);
	void operator=(Machine const&);
public:
	static Machine& getInstance(){
		static Machine instance;
		return instance;
	};
	void initialize(){
		motorL=Motor(TIM4,&(TIM8->CCR1),&(TIM8->CCR2));
		motorR=Motor(TIM3,&(TIM8->CCR4),&(TIM8->CCR3));
		initializeVariables();
		Kp_p=20.f;
		Kp_i=1.f;
		Kp_d=40.f;
		Kra_p=1.f;
		Kra_d=2.f;
		Ka_p=5.f;
		Ka_i=0.03f;
		Ka_d=10.f;
		Th_f=1280;
		Th_s=960;
		Offs_a1=1024;
		Offs_a2=200;
		Offs_c1=2048;
		Offs_c2=8192;
		activeflag=false;
		wallCtrlFlag=false;

		xLogger=new Logger(1024);
		yLogger=new Logger(1024);
		angleLogger=new Logger(1024);
		txLogger=new Logger(1024);
		tyLogger=new Logger(1024);
		rangleLogger=new Logger(1024);
		eaLogger=new Logger(1024);
		aiLogger=new Logger(1024);
		rvLogger=new Logger(1024);
		romegaLogger=new Logger(1024);

		isLogging=false;
	}
	void initializeVariables(){
		angle=0;
		omega=0;
		x=0;
		y=0;
		v=0;
		e=0;
		tangle=0;
		tx=0;
		ty=0;
		time=0;
		a_i=0;
		e_i=0;
		olda=0;
		olde=0;
		oldea=0;
		oldgzi=0;
		gx=gy=gz=0;
		oldgx=oldgy=oldgz=0;
		ax=ay=az=0;
		oldax=olday=oldaz=0;
		gx_i=0;
		gy_i=0;
		gz_i=0;
		ax_i=0;
		ay_i=0;
		az_i=0;
		fdest=Position();
		isLogging=false;
	}
	void MachineService();
	void setDestinationInMilliMeter(long _x,long _y,int _angle,unsigned int _time);
	void setDestinationInMilliMeter(Position dest,unsigned int _time,MotionFunctor *mf,bool isWallCtrlActive);
	void setDestination(long x,long y,long angle,unsigned int time);
    void setDestination(Position dest,unsigned int _time,MotionFunctor *mf,bool isWallCtrlActive);
	void setMotorMillimeterPerSec(int l,int r);

	const Position getFinalDestination() const { return fdest; }
	const Position getFinalDestinationInMilliMeter() const {
		Position d;
		d.x=MOTOR_CONST*(float)fdest.x;
		d.y=MOTOR_CONST*(float)fdest.y;
		d.angle=ANGLE_CONST_DEG*(float)fdest.angle;
		return d;
	}

	long getE() const{ return e; }
	long getX() const{ return x; }
	long getEX() const{ return tx-x; }
	long getY() const{ return y; }
	long getEY() const{ return ty-y; }
	long getAngle() const{ return angle; }
	long getEA() const{ return tangle-angle; }
	long getV() const{ return v; }
	long getOmega() const{ return omega; }
	long getAI() const{ return a_i; }
	long getEI() const{ return e_i; }
	int getVL() const{ return motorL.getV(); }
	int getVR() const{ return motorR.getV(); }

	int getGX() const{ return gx; }
	int getGY() const{ return gy; }
	int getGZ() const{ return gz; }
	long getGXI() const{ return gx_i; }
	long getGYI() const{ return gy_i; }
	long getGZI() const{ return gz_i; }
	int getAX() const{ return ax; }
	int getAY() const{ return ay; }
	int getAZ() const{ return az; }

	Logger *getGyroLogger() const{ return angleLogger; }
	bool isTargetQueueEmpty() const { return targetQueue.size()==0; }
	bool isMoving() const{ return time>0; }
	void setMotorPWMOffset(uint8_t _offset);
	void setMachinePID(float _K_p,float _K_i,float _K_d);
	void setRAnglePD(float _K_p,float _K_d);
	void setAnglePID(float _K_p,float _K_i,float _K_d);
	void setMotorPID(float _K_p,float _K_i,float _K_d);
	void setOffsetConst(uint16_t _Th_f,uint16_t _Th_s,uint16_t _Offs_a1,uint16_t _Offs_a2,uint16_t _Offs_c1,uint16_t _Offs_c2){
		Th_f=_Th_f;
		Th_s=_Th_s;
		Offs_a1=_Offs_a1;
		Offs_a2=_Offs_a2;
		Offs_c1=_Offs_c1;
		Offs_c2=_Offs_c2;
	}
	uint16_t getTHF() const { return Th_f; }
	uint16_t getTHS() const { return Th_s; }
	float getKAP() const { return Ka_p; }
	float getKAI() const { return Ka_i; }
	float getKAD() const { return Ka_d; }
	uint16_t getOffsA1() const { return Offs_a1; }
	uint16_t getOffsA2() const { return Offs_a2; }
	uint16_t getOffsC1() const { return Offs_c1; }
	uint16_t getOffsC2() const { return Offs_c2; }

	void printXLog() const { xLogger->print(); }
	void printYLog() const { yLogger->print(); }
	void printAngleLog() const { angleLogger->print(); }
	void printAllLog() const;
	void activate();
	void deactivate();
	void calibrateSensors();
	void readSensors();

	void setWallCtrl(bool state){ wallCtrlFlag=state; }
	void setLoggingState(bool state){ isLogging=state; }
};

extern "C" void MachineReadSensors();

#endif /* MACHINE_H_ */
