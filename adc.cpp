/*
 * adc.cpp
 *
 *  Created on: 2013/07/17
 *      Author: yuichi
 */

#include "stm32f4xx_conf.h"
#include "adc.h"

extern "C" {
	__IO uint16_t ADC_ConvVal[8]={};
	uint8_t ADC_Status=0;
}

void ADC_ReadAll(){
	GPIO_SetBits(GPIOB,GPIO_Pin_14|GPIO_Pin_15);
	for (volatile int i = 0; i < 1000; i++);
	ADC_Read();
	GPIO_ResetBits(GPIOB,GPIO_Pin_14|GPIO_Pin_15);
}

void ADC_Read(){
	ADC_SoftwareStartConv(ADC1);
	for(volatile int i=0;i<80;i++);
}

void ADC_Enable(){
	ADC_Status=1;
}

void ADC_Disable(){
	ADC_Status=0;
}

void ADC_Config(){
	DMA_InitTypeDef DMA_InitStruct;
	DMA_InitStruct.DMA_Channel=DMA_Channel_0;
	DMA_InitStruct.DMA_Memory0BaseAddr=(uint32_t)ADC_ConvVal;
	DMA_InitStruct.DMA_PeripheralBaseAddr=(uint32_t)0x40012308;
	DMA_InitStruct.DMA_DIR=DMA_DIR_PeripheralToMemory;
	DMA_InitStruct.DMA_BufferSize=8;
	DMA_InitStruct.DMA_PeripheralInc=DMA_PeripheralInc_Disable;
	DMA_InitStruct.DMA_MemoryInc=DMA_MemoryInc_Enable;
	DMA_InitStruct.DMA_PeripheralDataSize=DMA_PeripheralDataSize_HalfWord;
	DMA_InitStruct.DMA_MemoryDataSize=DMA_MemoryDataSize_HalfWord;
	DMA_InitStruct.DMA_Mode=DMA_Mode_Circular;
	DMA_InitStruct.DMA_Priority=DMA_Priority_High;
	DMA_InitStruct.DMA_FIFOMode=DMA_FIFOMode_Enable;
	DMA_InitStruct.DMA_FIFOThreshold=DMA_FIFOThreshold_HalfFull;
	DMA_InitStruct.DMA_MemoryBurst=DMA_MemoryBurst_Single;
	DMA_InitStruct.DMA_PeripheralBurst=DMA_PeripheralBurst_Single;
	DMA_Init(DMA2_Stream0,&DMA_InitStruct);
	DMA_Cmd(DMA2_Stream0,ENABLE);

	ADC_CommonInitTypeDef ADC_CommonInitStruct;
	ADC_CommonInitStruct.ADC_Mode = ADC_DualMode_RegSimult;
	ADC_CommonInitStruct.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
	ADC_CommonInitStruct.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStruct.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_20Cycles;
	ADC_CommonInit(&ADC_CommonInitStruct);

	ADC_InitTypeDef ADC_InitStruct;
	ADC_StructInit(&ADC_InitStruct);
	ADC_InitStruct.ADC_ScanConvMode=ENABLE;
	ADC_InitStruct.ADC_ContinuousConvMode=DISABLE;
	ADC_InitStruct.ADC_DataAlign=ADC_DataAlign_Right;
	ADC_InitStruct.ADC_ExternalTrigConvEdge=ADC_ExternalTrigConvEdge_None;
	ADC_InitStruct.ADC_NbrOfConversion=4;
	ADC_Init(ADC1, &ADC_InitStruct);
	ADC_Init(ADC2, &ADC_InitStruct);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_0,1,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_2,2,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_0,3,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC1,ADC_Channel_2,4,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_1,1,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_3,2,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_1,3,ADC_SampleTime_28Cycles);
	ADC_RegularChannelConfig(ADC2,ADC_Channel_3,4,ADC_SampleTime_28Cycles);
	ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);

	ADC_DMACmd(ADC1,ENABLE);

	ADC_Cmd(ADC1, ENABLE);
	ADC_Cmd(ADC2, ENABLE);
}
