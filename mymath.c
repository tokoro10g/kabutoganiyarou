/*
 * calc.c
 *
 *  Created on: 2013/03/10
 *      Author: yuichi
 */

#include "mymath.h"
#include "usart.h"
#include "hdsp2000lp.h"

const float WHEEL_R=24.7f;
const float GEAR_RATIO=4.5f;
const float ENC_NPULSE=1024.f;
const float MOTOR_CONST=(WHEEL_R*PI/ENC_NPULSE/GEAR_RATIO);
const float CMOTOR_CONST=(GEAR_RATIO*ENC_NPULSE/WHEEL_R/PI);
const float D=72.5f;
const float ANGLE_CONST= (MOTOR_CONST/D);
const float ANGLE_CONST_DEG= (WHEEL_R*180.f/D/ENC_NPULSE/GEAR_RATIO);
const float CANGLE_CONST= (CMOTOR_CONST*D);
const float CANGLE_CONST_DEG= (D*ENC_NPULSE*GEAR_RATIO/WHEEL_R/180.f);
const float GYRO_CONST=31600.f;
//const float GYRO_CONST=31730.f;
const float GYRO_ANGLE_CONST= (GYRO_CONST*WHEEL_R/ENC_NPULSE/GEAR_RATIO/D);
const float CGYRO_ANGLE_CONST= (ENC_NPULSE*GEAR_RATIO*D/WHEEL_R/GYRO_CONST);

//int(sin(index / (pi / 2) * 128) * 65535 + 0.5)
const unsigned short sin_table[] = {
    0,   804,  1608,  2412,  3216,  4019,  4821,  5623,
 6424,  7223,  8022,  8820,  9616, 10411, 11204, 11996,
12785, 13573, 14359, 15142, 15924, 16703, 17479, 18253,
19024, 19792, 20557, 21319, 22078, 22834, 23586, 24334,
25079, 25820, 26557, 27291, 28020, 28745, 29465, 30181,
30893, 31600, 32302, 32999, 33692, 34379, 35061, 35738,
36409, 37075, 37736, 38390, 39039, 39682, 40319, 40950,
41575, 42194, 42806, 43411, 44011, 44603, 45189, 45768,
46340, 46905, 47464, 48014, 48558, 49095, 49624, 50145,
50659, 51166, 51664, 52155, 52638, 53113, 53580, 54039,
54490, 54933, 55367, 55794, 56211, 56620, 57021, 57413,
57797, 58171, 58537, 58895, 59243, 59582, 59913, 60234,
60546, 60850, 61144, 61429, 61704, 61970, 62227, 62475,
62713, 62942, 63161, 63371, 63571, 63762, 63943, 64114,
64276, 64428, 64570, 64703, 64826, 64939, 65042, 65136,
65219, 65293, 65357, 65412, 65456, 65491, 65515, 65530,
65535, 0
};

float mysin(float x){
    long ix, subix, sign, tval;

    ix = (int)(x * (I_PI / PI));
    sign = ix & I_PI;
    ix &= (I_PI - 1);
    if(ix > I_HPI) ix = I_PI - ix;

    subix = ix & (SUBINDEX - 1);
    ix >>= SUBBIT;

    tval = ((long)sin_table[ix]   * (SUBINDEX - subix)
          + (long)sin_table[ix+1] * subix);

    return (sign ? -tval : tval) / (SUBINDEX * 65535.f);
}

float mycos(float x){
	return mysin(x+PI/2);
}

float myfabs(float x){
	return (x>=0)?x:-x;
}

float myfabsmax(float a,float b){
	return (myfabs(a)>myfabs(b))?a:b;
}

float myfabsmin(float a,float b){
	return (myfabs(a)<myfabs(b))?a:b;
}

float myhypot(float a, float b){
	a = myfabs(a);
	b = myfabs(b);
	if (a < b){
		float c=a;
		a=b;
		b=c;
	}
	if (b == 0)  return a;
	const int ITERATION_NUMBER = 3;
	float s;
	for (int i=0; i<ITERATION_NUMBER; i++){
		s=(b/a)*(b/a);
		s/=4.0f+s;
		a+=2.0f*a*s;
		b*=s;
	}
	return a;
}

float myatan2(float y, float x) // in rad
{
	if(ABS(x)<2.f&&ABS(y)<2.f) return 0.f;
	if(ABS(x)<2.f) return (float)SIGN(y)*PI/2.f;
#define fp_is_neg(val) ((((uint8_t*)&val)[3] & 0x80) != 0)
	float z = y / x;
	int16_t zi = ABS((int16_t)(z * 100));
	int8_t y_neg = fp_is_neg(y);
	if ( zi < 100 ){
		if (zi > 10)
			z = z / (1.0f + 0.28f * z * z);
		if (fp_is_neg(x)) {
			if (y_neg) z -= PI;
			else z += PI;
		}
	} else {
		z = (PI / 2.0f) - z / (z * z + 0.28f);
		if (y_neg) z -= PI;
	}
	//z *= (180.0f / PI);
	return z;
}

float mytanh(float x){
	float xa = ABS(x);
	float x2 = xa * xa;
	float x3 = xa * x2;
	float x4 = x2 * x2;
	float x7 = x3 * x4;
	float res = (1.0f - 1.0f / (1.0f + xa + x2 + 0.58576695f * x3 + 0.55442112f * x4 + 0.057481508f * x7));
	return (x > 0 ? res : -res);
}

unsigned int mymod(int a,int b){
	int c=a%b;
	return (c<0)?(c+b):c;
}
