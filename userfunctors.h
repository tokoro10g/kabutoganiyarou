/*
 * userfunctors.h
 *
 *  Created on: 2013/10/26
 *      Author: yuichi
 */

#ifndef USERFUNCTORS_H_
#define USERFUNCTORS_H_

#include "mymath.h"
#include "position.h"

#include <vector>

class EasingPoly3 : public EasingFunctor{
public:
	EasingPoly3():EasingFunctor(){}
	EasingPoly3(float _time):EasingFunctor(_time){}
	EasingPoly3(const EasingPoly3& obj){
		time=obj.time;
	}
	~EasingPoly3(){}
	virtual float func(unsigned int s){
		return (-2.f*(float)s+3.f*time)*(float)(s*s)/(time*time);
	}
};

class EasingPoly3In : public EasingFunctor{
public:
	EasingPoly3In():EasingFunctor(){}
	EasingPoly3In(float _time):EasingFunctor(_time){}
	EasingPoly3In(const EasingPoly3In& obj){
		time=obj.time;
	}
	~EasingPoly3In(){}
	virtual float func(unsigned int s){
		if(s>=time*0.5f){ return (float)s; }
		return (-2.f*(float)s+3.f*time)*(float)(s*s)/(time*time);
	}
};

class EasingPoly3Out : public EasingFunctor{
public:
	EasingPoly3Out():EasingFunctor(){}
	EasingPoly3Out(float _time):EasingFunctor(_time){}
	EasingPoly3Out(const EasingPoly3Out& obj){
		time=obj.time;
	}
	~EasingPoly3Out(){}
	virtual float func(unsigned int s){
		if(s<time*0.5f){ return (float)s; }
		return (-2.f*(float)s+3.f*time)*(float)(s*s)/(time*time);
	}
};

class EasingPoly3WithPause : public EasingFunctor{
public:
	EasingPoly3WithPause():EasingFunctor(){}
	EasingPoly3WithPause(float _time):EasingFunctor(_time){}
	EasingPoly3WithPause(const EasingPoly3WithPause& obj){
		time=obj.time;
	}
	~EasingPoly3WithPause(){}
	virtual float func(unsigned int s){
		if(s<=time*0.25f){
			return 0;
		} else if(s>time*0.25f&&s<time*0.75f){
			return (-2.f*(float)(s-time*0.25f)+3.f*time)*(float)(2.f*(s-time*0.25f)*(s-time*0.25f))/(time*time);
		} else {
			return time;
		}
	}
};

class EasingLinear : public EasingFunctor{
public:
	EasingLinear():EasingFunctor(){}
	EasingLinear(float _time):EasingFunctor(_time){}
	EasingLinear(const EasingLinear& obj){
		time=obj.time;
	}
	~EasingLinear(){}
	virtual float func(unsigned int s){
		return (float)s;
	}
};

class EasingPoly4 : public EasingFunctor{
private:
	float a,b,c,d;
public:
	EasingPoly4():EasingFunctor(){}
	EasingPoly4(float _time):EasingFunctor(_time){}
	EasingPoly4(float _time,float y0,float yt,float v0,float vt,float a0):EasingFunctor(_time/200.f){
		a=(3.f*y0+2.f*v0*time+a0*0.5f*time*time+vt*time-3.f*yt)/time/time/time/(yt-y0);
		b=(4.f*yt-4.f*y0-vt*time-3.f*v0*time-a0*time*time)/time/time/(yt-y0);
		c=a0*0.5f*time/(yt-y0);
		d=v0*time/(yt-y0);
	}
	EasingPoly4(const EasingPoly4& obj){
		time=obj.time;
		a=obj.a;
		b=obj.b;
		c=obj.c;
		d=obj.d;
	}
	~EasingPoly4(){}
	virtual float func(unsigned int s){
		float sf=(float)s/200.f;
		return 200.f*(a*sf*sf*sf*sf+b*sf*sf*sf+c*sf*sf+d*sf);
	}
};

class MotionLinear : public MotionFunctor{
private:
	Position diff;
public:
	MotionLinear(EasingFunctor *_e):MotionFunctor(_e){}
	MotionLinear(Position _origin,Position _dest,float _time,EasingFunctor *_e):MotionFunctor(_origin,_dest,_time,_e){
		diff=*dest-*origin;
	}
	MotionLinear(const MotionLinear& obj){
		origin=obj.origin;
		dest=obj.dest;
		diff=*dest-*origin;
		time=obj.time;
		e=obj.e;
	}
	~MotionLinear(){}
	virtual Position func(unsigned int s){
		float t=e->func(s);
		return *origin+(diff*(t/time));
	}
	virtual void initialize(){
		diff=*dest-*origin;
	}
};

class MotionArc : public MotionFunctor{
private:
	long rx;
	long ry;
	float cosoa;
	float sinoa;
public:
	MotionArc(EasingFunctor *_e):MotionFunctor(_e){}
	MotionArc(Position _origin,Position _dest,float _time,EasingFunctor *_e,long _cangle):MotionFunctor(_origin,_dest,_time,_e){
		Position dr=*dest-*origin;
		long ix=dr.x;
		long iy=dr.y;
		cosoa=mycos((float)origin->angle*ANGLE_CONST);
		sinoa=mysin((float)origin->angle*ANGLE_CONST);
		rx=cosoa*(float)ix+sinoa*(float)iy;
		ry=-sinoa*(float)ix+cosoa*(float)iy;
	}
	MotionArc(const MotionArc& obj){
		origin=obj.origin;
		dest=obj.dest;
		time=obj.time;
		e=obj.e;
		rx=obj.rx;
		ry=obj.ry;
		cosoa=obj.cosoa;
		sinoa=obj.sinoa;
	}
	~MotionArc(){}
	virtual Position func(unsigned int s){
		float t=e->func(s);
		long oangle=origin->angle;
		long pangle=(long)((dest->angle-origin->angle)*t/time);
		long px=-(long)((float)SIGN(pangle)*ABS((float)rx*(1.f-mycos((float)pangle*ANGLE_CONST))));
		long py=ABS((long)((float)ry*mysin((float)pangle*ANGLE_CONST)));
		long panglea=pangle+oangle;
		long pxa=cosoa*(float)px-sinoa*(float)py+origin->x;
		long pya=sinoa*(float)px+cosoa*(float)py+origin->y;
		return Position(pxa,pya,panglea);
	}
	virtual void initialize(){
		Position dr=*dest-*origin;
		rx=dr.x;
		ry=dr.y;
		cosoa=mycos((float)origin->angle*ANGLE_CONST);
		sinoa=mysin((float)origin->angle*ANGLE_CONST);
	}
};

class MotionCurve : public MotionFunctor{
private:
	std::vector<Position> posTable;
public:
	MotionCurve():MotionFunctor(new EasingLinear()){}
	MotionCurve(EasingFunctor *_e):MotionFunctor(_e){}
	MotionCurve(Position _origin,Position _dest,float _time,EasingFunctor *_e):MotionFunctor(_origin,_dest,_time,_e){}
	MotionCurve(const MotionCurve& obj){
		origin=obj.origin;
		dest=obj.dest;
		time=obj.time;
		e=obj.e;
	}
	~MotionCurve(){}
	virtual Position func(unsigned int s){
		//float t=e->func(s);
		return posTable[s];
	}
	virtual void initialize(){
		float diffx=ABS(dest->x-origin->x);
		float diffy=ABS(dest->y-origin->y);
		float diffa=dest->angle-origin->angle;
		while(diffa<-PI*CANGLE_CONST){
			diffa+=2.f*PI*CANGLE_CONST;
		}
		while(diffa>PI*CANGLE_CONST){
			diffa-=2.f*PI*CANGLE_CONST;
		}
		float sign;
		if(diffa<0){
			sign=-1.f;
		} else {
			sign=1.f;
		}
		float	x=origin->x,
				y=origin->y,
				a=origin->angle;
		for(int i=0;i<time+1;i++){
			posTable.push_back(Position(x,y,a));
			a=origin->angle*ANGLE_CONST+sign*(PI/2.f*(mytanh(2.f*0.5f*PI*(float)i/(float)time-PI*0.5f)/mytanh(PI*0.5f)+1.f)/2.f);
			x+=-mysin(a)*1.055f*PI/(float)time/2.f*diffx;
			y+=mycos(a)*1.055f*PI/(float)time/2.f*diffy;
			a*=CANGLE_CONST;
		}
	}
};

#endif /* USERFUNCTORS_H_ */
