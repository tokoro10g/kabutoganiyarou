/*
 * motor.cpp
 *
 *  Created on: 2013/06/14
 *      Author: yuichi
 */

#include "stm32f4xx.h"
#include "stm32f4xx_conf.h"

#include "motor.h"

#include "mymath.h"

void Motor::setMilliMeterPerSec(int val){
	milliMeterPerSec=val;
}

void Motor::MotorService(){
	nowX=TIM_Enc->CNT;
	nowV=(int)((long)nowX-(long)prvX+(((long)nowX-(long)prvX<-50000)?65536:0)-(((long)nowX-(long)prvX>50000)?65536:0));
	prvX=nowX;
	nowE=(float)milliMeterPerSec-(MOTOR_CONST*50.f*(float)nowV);
	V_i=V_i*0.98f+nowE;
	V_d=nowE-prvE;
	prvV=nowV;
	prvE=nowE;
	u=K_p*nowE+K_i*(float)V_i+K_d*(float)V_d;
	*(CCR_B)=(u>0)?(uint32_t)(1000-u)-PWM_Offset:1000;
	*(CCR_F)=(u<0)?(uint32_t)(1000+u)-PWM_Offset:1000;
}

void Motor::setMotorPID(float _K_p,float _K_i,float _K_d){
	K_p=_K_p;
	K_i=_K_i;
	K_d=_K_d;
}

void Motor::initialize(){
	TIM_Enc->CNT=0;
	prvX=0;
	nowX=0;
	prvE=0;
	nowE=0;
	u=0;
	prvV=0;
	nowV=0;
	milliMeterPerSec=0;
	V_i=0,V_d=0,V=0;
}

void Motor::eStop(){
	*(CCR_B)=0;
	*(CCR_F)=0;
}
