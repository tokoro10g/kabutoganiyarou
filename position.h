/*
 * position.h
 *
 *  Created on: 2013/10/25
 *      Author: yuichi
 */

#ifndef POSITION_H_
#define POSITION_H_

class Position {
private:
public:
	long x;
	long y;
	long angle;
	Position():x(0),y(0),angle(0){}
	Position(const Position &p){
		x=p.x;
		y=p.y;
		angle=p.angle;
	}
	Position(long _x,long _y,long _angle):x(_x),y(_y),angle(_angle){}
	void setVars(long _x,long _y,long _angle){ x=_x; y=_y; angle=_angle; }
	Position operator- (const Position& rhs){ return Position(x-rhs.x,y-rhs.y,angle-rhs.angle); }
	Position operator+ (const Position& rhs){ return Position(x+rhs.x,y+rhs.y,angle+rhs.angle); }
	template <typename T> Position operator/ (const T d){ return Position((T)x/d,(T)y/d,(T)angle/d); }
	template <typename T> Position operator* (const T d){ return Position((T)x*d,(T)y*d,(T)angle*d); }
};

#endif /* POSITION_H_ */
