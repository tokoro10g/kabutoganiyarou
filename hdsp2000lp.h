/*
 * hdsp2000lp.h
 *
 *  Created on: 2012/12/04
 *      Author: Tokoro
 */

#ifndef HDSP2000LP_H_
#define HDSP2000LP_H_

#ifdef __cplusplus
 extern "C" {
#endif
/* local macros */
#define HDSP_COL_GPIO GPIOA			// GPIOx is GPIOB
#define HDSP_COL_PIN(x) 1<<(x+8)	// COLx (x=[0:4]) are assigned to PB(x+8)
#define HDSP_CLK_GPIO GPIOB			// GPIOx is GPIOB
#define HDSP_CLK_PIN 1<<9			// CLK is assigned to PB9
#define HDSP_DAT_GPIO GPIOB			// GPIOx is GPIOB
#define HDSP_DAT_PIN 1<<8			// DAT is assigned to PB8

/* local constants */
#define HDSP_ASCII 0
#define HDSP_NIHONGO 1
#define HDSP_MINIHEX 2
#define HDSP_STATUS_ENABLE 1
#define HDSP_STATUS_DISABLE 0

/* function prototypes */
void HDSP_SetStatus(uint8_t newstatus);
void HDSP_LoadStr(const char *str,uint8_t flag);
void HDSP_LoadInt(uint16_t n);
void HDSP_LoadMiniHex(uint32_t word);
void HDSP_Refresh();
#ifdef __cplusplus
 }
#endif

#endif /* HDSP2000LP_H_ */
