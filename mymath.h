/*
 * mymath.h
 *
 *  Created on: 2013/03/10
 *      Author: yuichi
 */

#ifndef MYMATH_H_
#define MYMATH_H_

#include "stm32f4xx.h"
#ifdef __cplusplus
extern "C" {
#endif
#define PI (3.14159265f)

/* Wheel */
//#define WHEEL_R (25.f)
extern const float WHEEL_R;
extern const float GEAR_RATIO;		//36/8
extern const float ENC_NPULSE;	//256*4
inline float TRANS_NTOL(float x){ return (WHEEL_R*PI*((float)(x))/ENC_NPULSE/GEAR_RATIO); }

extern const float MOTOR_CONST;
extern const float CMOTOR_CONST;
extern const float D;
extern const float ANGLE_CONST;
extern const float ANGLE_CONST_DEG;
extern const float CANGLE_CONST;
extern const float CANGLE_CONST_DEG;
extern const float GYRO_CONST;
extern const float GYRO_ANGLE_CONST;
extern const float CGYRO_ANGLE_CONST;

#define SIGN(x) ((x>0)*2-((x)!=0))
#define ABS(x) ((x>0)?(x):-(x))

#define TABLESIZE 128 // suppose to be 2^n
#define SUBBIT    8
#define SUBINDEX  (1 << SUBBIT)
#define I_PI      (TABLESIZE * SUBINDEX * 2)
#define I_HPI     (TABLESIZE * SUBINDEX)

float mysin(float x);
float mycos(float x);
float myfabs(float x);
float myfabsmax(float a,float b);
float myfabsmin(float a,float b);
float myhypot(float a,float b);
float myatan2(float y, float x);
float mytanh(float x);
unsigned int mymod(int a,int b);
#ifdef __cplusplus
}
#endif
#endif /* MYMATH_H_ */
