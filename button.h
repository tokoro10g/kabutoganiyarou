/*
 * button.h
 *
 *  Created on: 2013/03/03
 *      Author: yuichi
 */

#ifndef BUTTON_H_
#define BUTTON_H_
#include "stm32f4xx.h"

#ifdef __cplusplus
 extern "C" {
#endif
void Button_Config(void);
void Button_U1_Event(void);
void Button_U2_Event(void);
#ifdef __cplusplus
 }
#endif
#endif /* BUTTON_H_ */
