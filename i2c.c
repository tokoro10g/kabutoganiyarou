/*
 * i2c.c
 *
 *  Created on: 2013/02/13
 *      Author: yuichi
 */

#include "i2c.h"
#include "usart.h"

uint8_t I2C_ErrFlag=0;

void I2C_Config() {
	RCC_AHB1PeriphClockCmd(I2Cx_RCC_IO, ENABLE);
	RCC_APB1PeriphClockCmd(I2Cx_RCC, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;
	/* I2C */
	GPIO_InitStruct.GPIO_Pin = I2Cx_SCL | I2Cx_SDA;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_OD;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(I2Cx_GPIO, &GPIO_InitStruct);
	GPIO_PinAFConfig(I2Cx_GPIO, I2Cx_PinSource_SCL, I2Cx_GPIO_AF );
	GPIO_PinAFConfig(I2Cx_GPIO, I2Cx_PinSource_SDA, I2Cx_GPIO_AF );

	I2C_InitTypeDef I2C_InitStruct;
	I2C_InitStruct.I2C_ClockSpeed = I2Cx_Speed;
	I2C_InitStruct.I2C_Mode = I2C_Mode_I2C;
	I2C_InitStruct.I2C_DutyCycle = I2C_DutyCycle_2;
	I2C_InitStruct.I2C_OwnAddress1 = 0x00;
	I2C_InitStruct.I2C_Ack = I2C_Ack_Enable;
	I2C_InitStruct.I2C_AcknowledgedAddress = I2C_AcknowledgedAddress_7bit;
	I2C_Init(I2Cx, &I2C_InitStruct);

	I2C_ITConfig(I2Cx,I2C_IT_ERR,ENABLE);

	I2C_Cmd(I2Cx, ENABLE);
}

uint8_t I2C_start(uint8_t address, uint8_t direction) {
	// Send I2C1 START condition
	I2C_GenerateSTART(I2Cx, ENABLE);

	// wait for I2C1 EV5 --> Slave has acknowledged start condition
	uint16_t timer=65535;
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT )){
		timer--;
		if(I2C_ErrFlag||!timer){
			I2C_ErrFlag=0;
			return 1;
		}
	}

	// Send slave Address for write
	I2C_Send7bitAddress(I2Cx, address, direction);

	/* wait for I2C1 EV6, check if
	 * either Slave has acknowledged Master transmitter or
	 * Master receiver mode, depending on the transmission
	 * direction
	 */
	timer=65535;
	if (direction == I2C_Direction_Transmitter ) {
		while (!I2C_CheckEvent(I2Cx,
				I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED )){
			timer--;
			if(I2C_ErrFlag||!timer){
				I2C_ErrFlag=0;
				return 1;
			}
		}
	} else if (direction == I2C_Direction_Receiver ) {
		while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED )){
			timer--;
			if(I2C_ErrFlag||!timer){
				I2C_ErrFlag=0;
				return 1;
			}
		}
	}
	return 0;
}

uint8_t I2C_write(uint8_t data) {
	I2C_SendData(I2Cx, data);
	// wait for I2C1 EV8_2 --> byte has been transmitted
	uint16_t timer=65535;
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTING )){
		timer--;
		if(I2C_ErrFlag||!timer){
			I2C_ErrFlag=0;
			return 1;
		}
	}
	return 0;
}

uint8_t I2C_read_ack(uint8_t *ptr) {
	// enable acknowledge of recieved data
	I2C_AcknowledgeConfig(I2Cx, ENABLE);
	// wait until one byte has been received
	uint16_t timer=65535;
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED )){
		timer--;
		if(I2C_ErrFlag||!timer){
			I2C_ErrFlag=0;
			return 1;
		}
	}
	// read data from I2C data register and return data byte
	uint8_t data = I2C_ReceiveData(I2Cx );
	*ptr=data;
	return 0;
}

uint8_t I2C_read_nack(uint8_t *ptr) {
	// disable acknowledge of received data
	I2C_AcknowledgeConfig(I2Cx, DISABLE);
	// wait until one byte has been received
	uint16_t timer=65535;
	while (!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED )){
		timer--;
		if(I2C_ErrFlag||!timer){
			I2C_ErrFlag=0;
			return 1;
		}
	}
	// read data from I2C data register and return data byte
	*ptr = I2C_ReceiveData(I2Cx );
	return 0;
}

void I2C_stop() {
	// Send I2C1 STOP Condition
	I2C_GenerateSTOP(I2Cx, ENABLE);
}

uint8_t I2C_WaitForBusy() {
	uint16_t timer=65535;
	while (I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY )!=RESET){
		timer--;
		if(I2C_ErrFlag||!timer){
			I2C_ErrFlag=0;
			return 1;
		}
	}
	return 0;
}

void I2C_AccCmd(uint8_t addr, uint8_t val) {
	I2C_WaitForBusy();
	I2C_start(I2C_ADDRESS_ACCEL, I2C_Direction_Transmitter );
	I2C_write(addr);
	I2C_write(val);
	I2C_stop();
}

void I2C_GyroCmd(uint8_t addr, uint8_t val) {
	I2C_WaitForBusy();
	I2C_start(I2C_ADDRESS_GYRO, I2C_Direction_Transmitter );
	I2C_write(addr);
	I2C_write(val);
	I2C_stop();
}

uint8_t I2C_ReadAccBase(uint8_t baseaddr,int *ptr){
	I2C_WaitForBusy();
	/*
	I2C_start(I2C_ADDRESS_ACCEL, I2C_Direction_Transmitter );
	I2C_write(baseaddr+1);
	I2C_start(I2C_ADDRESS_ACCEL, I2C_Direction_Receiver );
	uint8_t datah = I2C_read_nack();
	*/
	if(I2C_start(I2C_ADDRESS_ACCEL, I2C_Direction_Transmitter )){ return 1; }
	if(I2C_write(baseaddr|0x80)){ return 1; }
	if(I2C_start(I2C_ADDRESS_ACCEL, I2C_Direction_Receiver )){ return 1; }
	uint8_t datal,datah;
	if(I2C_read_ack(&datal)){;}
	if(I2C_read_nack(&datah)){;}
	I2C_stop();
	union {
		uint16_t u;
		int16_t i;
	} u2i;
	u2i.u=(datah<<8)|datal;
	*ptr=u2i.i;
	return 0;
}

uint8_t I2C_ReadGyroBase(uint8_t baseaddr,int *ptr){
	I2C_WaitForBusy();
	/*
	I2C_start(I2C_ADDRESS_GYRO, I2C_Direction_Transmitter );
	I2C_write(baseaddr+1);
	I2C_start(I2C_ADDRESS_GYRO, I2C_Direction_Receiver );
	uint8_t datah = I2C_read_nack();
	*/
	if(I2C_start(I2C_ADDRESS_GYRO, I2C_Direction_Transmitter )){ return 1; }
	if(I2C_write(baseaddr|0x80)){ return 1; }
	if(I2C_start(I2C_ADDRESS_GYRO, I2C_Direction_Receiver )){ return 1; }
	uint8_t datal,datah;
	if(I2C_read_ack(&datal)){ return 1;}
	if(I2C_read_nack(&datah)){ return 1;}
	I2C_stop();
	union {
		uint16_t u;
		int16_t i;
	} u2i;
	u2i.u=(datah<<8)|datal;
	*ptr=u2i.i;
	return 0;
}
