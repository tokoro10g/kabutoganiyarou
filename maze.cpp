#include <iostream>
#include <vector>
#include "maze.hpp"

const Direction Maze::DirFront={0x1};
const Direction Maze::DirRight={0x2};
const Direction Maze::DirBack={0x4};
const Direction Maze::DirLeft={0x8};

Maze::Maze(int _w):w(_w){
//	CellData cell={{0},{0},{0},{0}};
	data.clear();
	/*
	for(int i=0;i<w;i++){
		for(int j=0;j<w;j++){
			cell.wall.half=0;
			if(j==0&&i==w-1)
				cell.wall.bits.EAST=1;
			if(j==1&&i==w-1)
				cell.wall.bits.WEST=1;
			if(i==0)
				cell.wall.bits.NORTH=1;
			if(j==0)
				cell.wall.bits.WEST=1;
			if(j==w)
				cell.wall.bits.EAST=1;
			if(i==w)
				cell.wall.bits.SOUTH=1;
			data.push_back(cell);
		}
	}
	*/
}

void Maze::addCell(CellData v){
	data.push_back(v);
}

void Maze::setWall(const Coord& c,Direction dir){
	data[c.y*w+c.x].wall.half|=dir.half;

	if(dir.bits.NORTH&&c.y!=0){
		data[(c.y-1)*w+c.x].wall.bits.SOUTH=1;
	}
	else if(dir.bits.EAST&&c.x!=w-1){
		data[c.y*w+c.x+1].wall.bits.WEST=1;
	}
	else if(dir.bits.SOUTH&&c.y!=w-1){
		data[(c.y+1)*w+c.x].wall.bits.NORTH=1;
	}
	else if(dir.bits.WEST&&c.x!=0){
		data[c.y*w+c.x-1].wall.bits.EAST=1;
	}
}

void Maze::setChkWall(const Coord& c,Direction dir){
	data[c.y*w+c.x].chk_wall.half|=dir.half;

	if(dir.bits.NORTH&&c.y!=0){
		data[(c.y-1)*w+c.x].chk_wall.bits.SOUTH=1;
	}
	else if(dir.bits.EAST&&c.x!=w-1){
		data[c.y*w+c.x+1].chk_wall.bits.WEST=1;
	}
	else if(dir.bits.SOUTH&&c.y!=w-1){
		data[(c.y+1)*w+c.x].chk_wall.bits.NORTH=1;
	}
	else if(dir.bits.WEST&&c.x!=0){
		data[c.y*w+c.x-1].chk_wall.bits.EAST=1;
	}
}

bool Maze::isSetWall(const Coord& c,Direction dir) const{
	return data[c.y*w+c.x].wall.half&dir.half;
}
