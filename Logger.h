/*
 * Logger.h
 *
 *  Created on: 2013/10/27
 *      Author: yuichi
 */

#include <queue>

#ifndef LOGGER_H_
#define LOGGER_H_

class Logger{
private:
	std::queue<int> log;
	unsigned int size;
public:
	Logger();
	Logger(unsigned int _size):size(_size){}
	virtual ~Logger();
	void enqueue(int elem);
	int dequeue();
	void print();
	int getSize() const { return log.size(); }
};

#endif /* LOGGER_H_ */
