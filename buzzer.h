/*
 * buzzer.h
 *
 *  Created on: 2013/03/03
 *      Author: yuichi
 */

#ifndef BUZZER_H_
#define BUZZER_H_
#include "stm32f4xx.h"

#ifdef __cplusplus
 extern "C" {
#endif
void Buzzer_Config();
void Buzzer_SetStatus(uint8_t newstat);
void Buzzer_Add(uint16_t freq, uint16_t duration);
void Buzzer_SetFreq(uint16_t freq);
void Buzzer_Play();
void Buzzer_PlayEnchukei();
void Buzzer_Stop();

void Buzzer_Event();
#ifdef __cplusplus
 }
#endif
#endif /* BUZZER_H_ */
