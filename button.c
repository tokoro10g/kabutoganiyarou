/*
 * button.c
 *
 *  Created on: 2013/03/03
 *      Author: yuichi
 */

#include "button.h"
#include "usart.h"
#include "buzzer.h"
#include "service.h"
#include "hdsp2000lp.h"

extern SrvStat_t ServiceStatus;

static const uint16_t clickSound[8]={
		1760,
		1976,
		2093,
		2349,
		2637,
		2794,
		3136,
		3520
};

void Button_Config(void) {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM14, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;
	/* SWITCH */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOC, &GPIO_InitStruct);

	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM8_TRG_COM_TIM14_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 3;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	/* Switch detection by using a sampling method */
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_Period = 1000; // 10ms
	TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStruct.TIM_Prescaler = (uint16_t)(
			(SystemCoreClock / 2) / 100000) - 1; // 100kHz
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM14, &TIM_TimeBaseInitStruct);
	TIM_ITConfig(TIM14, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM14, ENABLE);
	//ServiceStatus.srvopt=0x1f;
}

void Button_U1_Event(void) {
	Buzzer_Add(clickSound[ServiceStatus.menuMode], 30);
	incServiceMode();
}

void Button_U2_Event(void) {
	//USART_Puts("User2\r\n");
	Buzzer_Add(1760, 30);
	Buzzer_Add(3520, 30);
	toggleMenu();
	//TIM3->CNT = 0;
}
