#pragma once

#include <vector>
#include <stdint.h>

typedef union{
	unsigned half:4;
	struct{
		unsigned NORTH:1;
		unsigned EAST:1;
		unsigned SOUTH:1;
		unsigned WEST:1;
	} bits;
} Direction;

typedef union{
	unsigned half:4;
	struct{
		unsigned ROUTE:1;
		unsigned START:1;
		unsigned CURRENT:1;
		unsigned END:1;
	} bits;
} CellFlag;

typedef union{
	unsigned half:4;
	struct{
		unsigned FLAG1:1;
		unsigned FLAG2:1;
		unsigned FLAG3:1;
		unsigned FLAG4:1;
	} bits;
} FlagSet;

typedef struct{
	Direction wall;
	CellFlag cflag;
	Direction chk_wall;
	FlagSet sflag;
} CellData;

typedef std::vector<CellData> MazeData;

struct Coord {
	int x;
	int y;
	Direction dir;
};

class Maze{
private:
	MazeData data;
	int w;
public:
	static const Direction DirFront;
	static const Direction DirRight;
	static const Direction DirBack;
	static const Direction DirLeft;
public:
	Maze(int _w);
	Maze(const Maze& m):data(m.data),w(m.w){}
	~Maze(){ data.clear(); }
	int getWidth() const{ return w; }
	void addCell(CellData v);
	const MazeData& getMazeData() const{ return data; }
	void setWall(const Coord& c,Direction dir);
	void setChkWall(const Coord& c,Direction dir);
	bool isSetWall(const Coord& c,Direction dir) const;
	bool isSearchedWall(const Coord& c,Direction dir) const;
	bool isSearchedCell(const Coord& c) const;
};
