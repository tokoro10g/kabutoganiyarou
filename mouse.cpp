#include <iostream>
#include <vector>
#include <cmath>
#include "mouse.hpp"
#include "adc.h"
#include "machine.h"
#include "userfunctors.h"
#include "hdsp2000lp.h"

const long Mouse::STEP_TIME;
const char* Mouse::dirchr=" ^> v   <";

static const int slr=40;
static const int slrf=90;
typedef enum{
	still,
	straight,
	turn,
	turnandstraight
} Motion;
static Motion prevMotion=still;

Mouse::Mouse(const Maze& newmaze,int width){
	w=width;
	c.dir.half=1;
	c.x=0;
	c.y=w-1;
	maze=new Maze(newmaze);
}

void Mouse::senseAndSetWall(const Maze& realMaze){
	if(senseRight(realMaze)){
		//std::cout<<"right,";
		setWall(transDirection(Maze::DirRight));
	}
	if(senseLeft(realMaze)){
		//std::cout<<"left,";
		setWall(transDirection(Maze::DirLeft));
	}
	if(senseFront(realMaze)){
		//std::cout<<"front,";
		setWall(transDirection(Maze::DirFront));
	}
}

void Mouse::senseAndSetWall(){
	USART_printf("L:%d,F:%d,R:%d\r\n",3,(ADC_ConvVal[2]+ADC_ConvVal[6])>>6,(ADC_ConvVal[1]+ADC_ConvVal[2]+ADC_ConvVal[5]+ADC_ConvVal[6])>>7,(ADC_ConvVal[3]+ADC_ConvVal[7])>>6);
	if(senseRight()){
		setWall(transDirection(Maze::DirRight));
	}
	if(senseLeft()){
		setWall(transDirection(Maze::DirLeft));
	}
	if(senseFront()){
		setWall(transDirection(Maze::DirFront));
	}
}

bool Mouse::senseFront(const Maze& realMaze) const{
	maze->setChkWall(c,transDirection(Maze::DirFront));
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(Maze::DirFront).half;
}
bool Mouse::senseRight(const Maze& realMaze) const{
	maze->setChkWall(c,transDirection(Maze::DirRight));
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(Maze::DirRight).half;
}
bool Mouse::senseLeft(const Maze& realMaze) const{
	maze->setChkWall(c,transDirection(Maze::DirLeft));
	return realMaze.getMazeData()[c.y*w+c.x].wall.half&transDirection(Maze::DirLeft).half;
}

bool Mouse::senseFront() const{
	Direction dir={1};
	maze->setChkWall(c,transDirection(dir));
	if(ADC_ConvVal[0]+ADC_ConvVal[1]+ADC_ConvVal[4]+ADC_ConvVal[5]>Machine::getInstance().getTHF()){
		return true;
	} else {
		return false;
	}
}

bool Mouse::senseRight() const{
	Direction dir={2};
	maze->setChkWall(c,transDirection(dir));
	if(ADC_ConvVal[3]+ADC_ConvVal[7]>Machine::getInstance().getTHS()){
		return true;
	} else {
		return false;
	}
}

bool Mouse::senseLeft() const{
	Direction dir={8};
	maze->setChkWall(c,transDirection(dir));
	if(ADC_ConvVal[2]+ADC_ConvVal[6]>Machine::getInstance().getTHS()){
		return true;
	} else {
		return false;
	}
}

bool Mouse::moveForward(){
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
	uint8_t d=c.dir.half;
	int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
	Position p(c.x*180,(15-c.y)*180,angle);
	p.x-=slr*(!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST));
	p.y+=slr*(!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH));
	switch(prevMotion){
		case still:
		case turn:
		case turnandstraight:
			Machine::getInstance().setDestinationInMilliMeter(p,55,new MotionLinear(new EasingLinear()),true);
			break;
		case straight:
			Machine::getInstance().setDestinationInMilliMeter(p,75,new MotionLinear(new EasingLinear()),true);
			break;
	}
	USART_Puts("Move Forward\r\n");
	prevMotion=straight;
	return true;
}
void Mouse::turnRightAndMoveForward(){
	c.dir.half=(c.dir.half<<1)&0xF;
	if(c.dir.half==0) c.dir.half=0x1;
	uint8_t d=c.dir.half;
	int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
	Position p(c.x*180,(15-c.y)*180,angle);
	p.x+=slr*(!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST));
	p.y-=slr*(!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH));
	Machine::getInstance().setDestinationInMilliMeter(p,40,new MotionArc(new EasingLinear()),false);
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
	p=Position(c.x*180,(15-c.y)*180,angle);
	p.x-=slr*(!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST));
	p.y+=slr*(!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH));
	Machine::getInstance().setDestinationInMilliMeter(p,40,new MotionLinear(new EasingLinear()),true);
	prevMotion=turnandstraight;
	USART_Puts("Turn Right\r\n");
}
void Mouse::turnLeftAndMoveForward(){
	c.dir.half=(c.dir.half>>1)&0xF;
	if(c.dir.half==0) c.dir.half=0x8;
	uint8_t d=c.dir.half;
	int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
	Position p(c.x*180,(15-c.y)*180,angle);
	p.x+=slr*(!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST));
	p.y-=slr*(!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH));
	Machine::getInstance().setDestinationInMilliMeter(p,40,new MotionArc(new EasingLinear()),false);
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
	p=Position(c.x*180,(15-c.y)*180,angle);
	p.x-=slr*(!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST));
	p.y+=slr*(!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH));
	Machine::getInstance().setDestinationInMilliMeter(p,40,new MotionLinear(new EasingLinear()),true);
	prevMotion=turnandstraight;
	USART_Puts("Turn Left\r\n");
}
void Mouse::turnRight(){
	c.dir.half=(c.dir.half<<1)&0xF;
	if(c.dir.half==0) c.dir.half=0x1;
	uint8_t d=c.dir.half;
	int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
	Position p=Machine::getInstance().getFinalDestinationInMilliMeter();
	p.angle=angle;
	switch(prevMotion){
		case still:
		case turnandstraight:
			Machine::getInstance().setDestinationInMilliMeter(p,65,new MotionLinear(new EasingPoly3In()),false);
			break;
		case turn:
		case straight:
			Machine::getInstance().setDestinationInMilliMeter(p,65,new MotionLinear(new EasingLinear()),false);
			break;
	}
	prevMotion=turn;
	USART_Puts("Turn Right\r\n");
}
void Mouse::turnLeft(){
	c.dir.half=(c.dir.half>>1)&0xF;
	if(c.dir.half==0) c.dir.half=0x8;
	uint8_t d=c.dir.half;
	int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
	Position p=Machine::getInstance().getFinalDestinationInMilliMeter();
	p.angle=angle;
	switch(prevMotion){
		case still:
		case turnandstraight:
			Machine::getInstance().setDestinationInMilliMeter(p,65,new MotionLinear(new EasingPoly3In()),false);
			break;
		case turn:
		case straight:
			Machine::getInstance().setDestinationInMilliMeter(p,65,new MotionLinear(new EasingLinear()),false);
			break;
	}
	prevMotion=turn;
	USART_Puts("Turn Left\r\n");
}
void Mouse::turnBack(){
	c.dir.half=((c.dir.half&0x3)<<2)|((c.dir.half&0xc)>>2);
	uint8_t d=c.dir.half;
	int angle=((d&2)>>1)*(-90)+((d&4)>>2)*(-180)+((d&8)>>3)*90;
	Position p=Machine::getInstance().getFinalDestinationInMilliMeter();
	p.angle=angle;
	switch(prevMotion){
		case still:
		case straight:
		case turnandstraight:
			Machine::getInstance().setDestinationInMilliMeter(p,160,new MotionLinear(new EasingPoly3WithPause()),false);
			break;
	}
	prevMotion=turn;
	USART_Puts("Turn Back\r\n");
}
bool Mouse::moveForwardVirtual(){
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
	return true;
}
void Mouse::turnRightAndMoveForwardVirtual(){
	c.dir.half=(c.dir.half<<1)&0xF;
	if(c.dir.half==0) c.dir.half=0x1;
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
}
void Mouse::turnLeftAndMoveForwardVirtual(){
	c.dir.half=(c.dir.half>>1)&0xF;
	if(c.dir.half==0) c.dir.half=0x8;
	c.x+=!!(c.dir.bits.EAST)-!!(c.dir.bits.WEST);
	c.y+=!!(c.dir.bits.SOUTH)-!!(c.dir.bits.NORTH);
}

void Mouse::generateFastestPath(const Route route){
	int firstFlag=1;
	int frontCnt=0;
	int index=route.front();
	int angle=0;
	for(Route::const_iterator it=route.begin()+1;it!=route.end();it++){
		int dir=0;
		int oldx=index%16;
		int oldy=index/16;
		int nowx=(*it)%16;
		int nowy=(*it)/16;

		USART_printf("route:%d\r\n",1,index);

		if((*it)==index-w){        //Route: NORTH
			dir=0;
		} else if((*it)==index+1){      //Route: EAST
			dir=270;
		} else if((*it)==index+w){ //Route: SOUTH
			dir=180;
		} else if((*it)==index-1){      //Route: WEST
			dir=90;
		} else {
			HDSP_LoadStr("ERRR",HDSP_ASCII);
			USART_Puts("Error in routing\r\n");
			return;
		}
		if(dir==mymod(angle,360)){
			frontCnt++;
			index=(*it);
			continue;
		} else  {
			Position p(oldx*180,(15-oldy)*180,angle);
			switch(mymod(angle,360)){
				case 0:
					p.y-=slrf;
					break;
				case 90:
					p.x+=slrf;
					break;
				case 180:
					p.y+=slrf;
					break;
				case 270:
					p.x-=slrf;
					break;
			}
			if(frontCnt>0){
				if(firstFlag){
					Machine::getInstance().setDestinationInMilliMeter(p,40.f*pow(frontCnt,0.76),new MotionLinear(new EasingPoly4(40.f*pow(frontCnt,0.76),0,180.f*(float)frontCnt,0.f,500.f,8000.f)),true);
					firstFlag=0;
				} else {
					Machine::getInstance().setDestinationInMilliMeter(p,30.f*pow(frontCnt,0.78),new MotionLinear(new EasingPoly4(30.f*pow(frontCnt,0.78),0,180.f*(float)frontCnt,500.f,500.f,8500.f)),true);
				}
				frontCnt=0;
			}
			p.x=oldx*180;
			p.y=(15-oldy)*180;
			if(dir==mymod(angle-90,360)){
				angle-=90;
				angle=mymod(angle,360);
				p.x+=slrf*(!!(angle==270)-!!(angle==90));
				p.y-=slrf*(!!(angle==180)-!!(angle==0));
				p.angle=angle;
				Machine::getInstance().setDestinationInMilliMeter(p,42,new MotionCurve(),true);
				//Machine::getInstance().setDestinationInMilliMeter(p,32,new MotionArc(new EasingLinear()),false);
			} else if(dir==mymod(angle+90,360)){
				angle+=90;
				angle=mymod(angle,360);
				p.x+=slrf*(!!(angle==270)-!!(angle==90));
				p.y-=slrf*(!!(angle==180)-!!(angle==0));
				p.angle=angle;
				Machine::getInstance().setDestinationInMilliMeter(p,42,new MotionCurve(),true);
				//Machine::getInstance().setDestinationInMilliMeter(p,32,new MotionArc(new EasingLinear()),false);
			}
/*
			p.x=nowx*180;
			p.y=(15-nowy)*180;
			p.x-=slrf*(!!(mymod(angle,360)==270)-!!(mymod(angle,360)==90));
			p.y+=slrf*(!!(mymod(angle,360)==180)-!!(mymod(angle,360)==0));
			Machine::getInstance().setDestinationInMilliMeter(p,4,new MotionLinear(new EasingLinear()),true);
			*/

		}
		index=(*it);
	}
	if(frontCnt>0){
		Position p(index%16*180,(15-index/16)*180,angle);
		Machine::getInstance().setDestinationInMilliMeter(p,50.f*pow(frontCnt,0.78),new MotionLinear(new EasingPoly4(50.f*pow(frontCnt,0.78),0,180.f*(float)frontCnt,500.f,0.f,8000.f)),true);
	}
}

void Mouse::setMaze(const Maze& newmaze){
	delete maze;
	maze=new Maze(newmaze);
}
void Mouse::clearMaze(){
	int width=maze->getWidth();
	delete maze;
	maze=new Maze(width);
	CellData cell={{0},{0},{0},{0}};
	for(int i=0;i<16;i++){
		for(int j=0;j<16;j++){
			cell.wall.half=0;
			if(j==0&&i==15)
				cell.wall.bits.EAST=1;
			if(j==1&&i==15)
				cell.wall.bits.WEST=1;
			if(i==0)
				cell.wall.bits.NORTH=1;
			if(j==0)
				cell.wall.bits.WEST=1;
			if(j==15)
				cell.wall.bits.EAST=1;
			if(i==15)
				cell.wall.bits.SOUTH=1;
			maze->addCell(cell);
		}
	}
}

void Mouse::setWall(Direction dir){
	maze->setWall(c,dir);
}

Direction Mouse::transDirection(const Direction local) const{
	Direction global=c.dir;
	Direction dir={0};
	if(local.bits.NORTH){
		return global;
	} else if(local.bits.EAST){
		dir.half=(((global.half&0x8)>>3)|(global.half<<1))&0xF;
		return dir;
	} else if(local.bits.WEST){
		dir.half=(((global.half&0x1)<<3)|(global.half>>1))&0xF;
		return dir;
	} else {
		std::cout<<"ouch"<<std::endl;
		return dir;
	}
}

const Route Mouse::searchMaze(int start,int goal,bool isSearching){
	int index;
	int hangCount=0;
	Route route;
	while((index=getCoordIndex())!=goal){
		Graph graph=Graph(*maze);
		graph.dijkstra(goal,index,isSearching);
		route=graph.getRoute(goal,index);
		for(Route::iterator it=route.begin()+1;it!=route.end();it++){
			Direction routeDir={0};
			if((*it)==index-w){        //Route: NORTH
				routeDir.half=1;
			} else if((*it)==index+1){      //Route: EAST
				routeDir.half=2;
			} else if((*it)==index+w){ //Route: SOUTH
				routeDir.half=4;
			} else if((*it)==index-1){      //Route: WEST
				routeDir.half=8;
			} else {
				USART_Puts("Error in routing\r\n");
				return Route();
			}
			if(hangCount==4){
				return Route();
			}
			if(isSearching){
				senseAndSetWall();
			}
			if(routeDir.half==transDirection(Maze::DirFront).half){
				if(isSearching){
					if(senseFront()){
						hangCount++;
						break;
					} else {
						hangCount=0;
						moveForward();
					}
				} else {
					moveForwardVirtual();
				}
			} else if(routeDir.half==transDirection(Maze::DirRight).half){
				if(isSearching){
					if(senseRight()){
						hangCount++;
						break;
					} else {
						hangCount=0;
						turnRightAndMoveForward();
					}
				} else {
					turnRightAndMoveForwardVirtual();
				}
			} else if(routeDir.half==transDirection(Maze::DirLeft).half){
				if(isSearching){
					if(senseLeft()){
						hangCount++;
						break;
					} else {
						hangCount=0;
						turnLeftAndMoveForward();
					}
				} else {
					turnLeftAndMoveForwardVirtual();
				}
			} else {
				hangCount=0;
				turnBack();
				moveForward();
			}
			if(isSearching){
				while(!Machine::getInstance().isTargetQueueEmpty());
			}
			index=getCoordIndex();
		}
	}
	return route;
}
