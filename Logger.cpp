/*
 * Logger.cpp
 *
 *  Created on: 2013/10/27
 *      Author: yuichi
 */

#include "Logger.h"
#include "usart.h"
#include "position.h"

Logger::Logger():size(32){}
Logger::~Logger(){}

void Logger::enqueue(int elem){
	log.push(elem);
	if(log.size()>size){
		log.pop();
	}
}

int Logger::dequeue(){
	int elem=log.front();
	log.pop();
	return elem;
}

void Logger::print(){
	while(log.size()>0){
		int elem=dequeue();
		USART_printf("%d\r\n",1,elem);
		for(volatile int i=0;i<20000;i++);
	}
}
