/*
 * service.c
 *
 *  Created on: 2013/03/14
 *      Author: yuichi
 */

#include "service.h"
#include "adc.h"
#include "mymath.h"
#include "hdsp2000lp.h"
#include "usart.h"
#include "Baselibc/string.h"
#include "Baselibc/stdio.h"
#include "buzzer.h"
#include "i2c.h"
#include "motor.h"
#include "machine.h"
#include "eeptools.h"
#include "maze.hpp"
#include "mouse.hpp"

#include "userfunctors.h"

// Constants
static const uint8_t MNUMode_MNU=0;
static const uint8_t MNUMode_ENC=1;
static const uint8_t MNUMode_CLB=2;
static const uint8_t MNUMode_GYR=3;
static const uint8_t MNUMode_ADC=4;
static const uint8_t MNUMode_BUZ=5;
static const uint8_t MNUMode_MTR=6;
static const uint8_t MNUMode_STG=7;

static const uint8_t ENCMode_LR=0;
static const uint8_t ENCMode_L=1;
static const uint8_t ENCMode_R=2;

static const uint8_t ACCMode_X=0;
static const uint8_t ACCMode_Y=1;
static const uint8_t ACCMode_Z=2;

static const uint8_t CLBMode_Retn=0;
static const uint8_t CLBMode_Thf=1;
static const uint8_t CLBMode_Ths=2;
static const uint8_t CLBMode_Oa1=3;
static const uint8_t CLBMode_Oa2=4;
static const uint8_t CLBMode_Oc1=5;
static const uint8_t CLBMode_Oc2=6;

static const uint8_t GYRMode_X=0;
static const uint8_t GYRMode_Y=1;
static const uint8_t GYRMode_Z=2;

static const uint8_t ADCMode_All=0;
static const uint8_t ADCMode_1=1;
static const uint8_t ADCMode_2=2;
static const uint8_t ADCMode_3=3;
static const uint8_t ADCMode_4=4;

static const uint8_t BUZMode_Enchukei=0;

static const uint8_t MTRMode_Stll=0;
static const uint8_t MTRMode_Rot=1;
static const uint8_t MTRMode_StY=2;
static const uint8_t MTRMode_Rec=3;
static const uint8_t MTRMode_Auto=4;
static const uint8_t MTRMode_Clk=5;
static const uint8_t MTRMode_Cir=6;

static const uint8_t STGMode_Retn=0;
static const uint8_t STGMode_HDSP=1;
static const uint8_t STGMode_Service=2;
static const uint8_t STGMode_Buzzer=3;

static const uint8_t Maze_Block=180;
static const uint8_t AD_Threshold=40;

extern bool AutoFlag;
extern Mouse *mouse;

volatile SrvStat_t ServiceStatus;
uint8_t toggleFlag = 0;
uint8_t oldmenuSrvMode = 0;
uint8_t srvCounter = 0;
uint8_t fivemsCounter = 0;
uint8_t secCounter = 0;
uint8_t minCounter = 0;
uint8_t hourCounter = 0;

int32_t targetAng=0;
int32_t targetX=0;
int32_t targetY=0;
uint8_t MTRState = 0;
uint8_t oldMTRState = 0;

uint16_t param[9];
uint16_t oldEnc;

char menuText[9][5] = { "RETN", "ENCC", "CLBR", "GYRC", "ADCC", "BUZC", "MTRC",
		"SETG", "RUN!" };

char settingText[3][5] = { "HD:E", "SR:E", "BZ:E" };

/**
 * @brief Increment the mode number.
 * This function is called by Button_U1_Event.
 * @param  None
 * @retval None
 */
void incServiceMode() {
	MTRState=0;
	targetAng=0;
	targetX=0;
	targetY=0;
	ServiceStatus.menuMode++;
	if (ServiceStatus.menuMode > 8)
		ServiceStatus.menuMode = 0;
}

void toggleMenu() {
	HDSP_SetStatus(HDSP_STATUS_ENABLE);
	if (ServiceStatus.srvMode == MNUMode_MNU) {
		ServiceStatus.srvMode = ServiceStatus.menuMode ? ServiceStatus.menuMode : ServiceStatus.oldSrvMode;
		if(ServiceStatus.srvMode==MNUMode_MTR){
			Machine::getInstance().activate();
		} else {
			Machine::getInstance().deactivate();
		}
		if(ServiceStatus.srvMode==MNUMode_CLB){
			param[0]=Machine::getInstance().getTHF();
			param[1]=Machine::getInstance().getTHS();
			param[2]=Machine::getInstance().getOffsA1();
			param[3]=Machine::getInstance().getOffsA2();
			param[4]=Machine::getInstance().getOffsC1();
			param[5]=Machine::getInstance().getOffsC2();
			param[6]=Machine::getInstance().getKAP()*100;
			param[7]=Machine::getInstance().getKAI()*100;
			param[8]=Machine::getInstance().getKAD()*100;
		}
		if(ServiceStatus.srvMode==MNUMode_ADC||ServiceStatus.srvMode==MNUMode_MTR){
			ADC_Enable();
		} else {
			ADC_Disable();
		}
		if(ServiceStatus.srvMode==MNUMode_CLB||ServiceStatus.srvMode==MNUMode_MTR){
			oldEnc=TIM3->CNT;
		}
		if (ServiceStatus.srvMode == MNUMode_MNU || ServiceStatus.srvMode == MNUMode_STG)
			HDSP_SetStatus(HDSP_STATUS_ENABLE);
		else
			HDSP_SetStatus((ServiceStatus.srvOpt & 1));
		ServiceStatus.menuMode = 0;
	} else if (ServiceStatus.srvMode == MNUMode_CLB) {
		if(ServiceStatus.menuMode == CLBMode_Retn) {
			EEP_Write(PARAM_WALL_FRONT_THRESHOLD,param[0]);
			EEP_Write(PARAM_WALL_SIDE_THRESHOLD,param[1]);
			EEP_Write(PARAM_OFFSET_ANGLE_ONE_WALL,param[2]);
			EEP_Write(PARAM_OFFSET_ANGLE_TWO_WALLS,param[3]);
			EEP_Write(PARAM_OFFSET_COORD_ONE_WALL,param[4]);
			EEP_Write(PARAM_OFFSET_COORD_TWO_WALLS,param[5]);
			EEP_WriteFloat(PARAM_ANG_P,(float)param[6]/100.f);
			EEP_WriteFloat(PARAM_ANG_I,(float)param[7]/100.f);
			EEP_WriteFloat(PARAM_ANG_D,(float)param[8]/100.f);
		} else {
			toggleFlag = 1;
		}
	} else if (ServiceStatus.srvMode == MNUMode_STG) {
		if (ServiceStatus.menuMode == STGMode_Retn) {
			Buzzer_SetStatus((ServiceStatus.srvOpt & 4) >> 2);
			if (ServiceStatus.srvOpt & 1) {
				HDSP_SetStatus(HDSP_STATUS_ENABLE);
			}
			if ((ServiceStatus.srvOpt & 2) == 0) {
				;
			}
			EEP_Write(18,ServiceStatus.srvOpt);
			ServiceStatus.srvMode = ServiceStatus.menuMode;
			ServiceStatus.menuMode = 0;
			ServiceStatus.oldSrvMode = ServiceStatus.srvMode;
		} else {
			toggleFlag = 1;
		}
	} else {
		ServiceStatus.menuMode = MNUMode_MNU;
		ServiceStatus.oldSrvMode = ServiceStatus.srvMode;
		ServiceStatus.srvMode = MNUMode_MNU;
	}
}

/**
 * @brief Service handler
 * This function is called periodically by TIM9.
 * @param  None
 * @retval None
 */
void ServiceHandler() {
	try{
		Machine::getInstance().MachineService();
	} catch (int e){
		switch(e){
			case -1:
				Machine::getInstance().deactivate();
				Buzzer_Add(988,30);
				Buzzer_Add(0,30);
				Buzzer_Add(988,30);
				Buzzer_Add(0,30);
				Buzzer_Add(988,30);
				break;
			default:
				break;
		}
	}

	switch (ServiceStatus.srvMode) {
		case MNUMode_MNU:
			HDSP_LoadStr(menuText[ServiceStatus.menuMode], HDSP_ASCII);
			break;
		case MNUMode_ENC:
			Service_ENCMode();
			break;
		case MNUMode_CLB:
			Service_CLBMode();
			break;
		case MNUMode_GYR:
			Service_GYRMode();
			break;
		case MNUMode_ADC:
			Service_ADCMode();
			break;
		case MNUMode_BUZ:
			Service_BUZZERMode();
			break;
		case MNUMode_MTR:
			Service_MTRMode();
			break;
		case MNUMode_STG:
			Service_STGMode();
			break;
		/*
		 case 8:
			 HDSP_LoadStr("RUN!",HDSP_ASCII);
			 if (toggleFlag) {

			 }
			 break;
		 */
		default:
			break;
	}
	toggleFlag = 0;
}

void Service_ENCMode(){
	if (ServiceStatus.menuMode > 2)
		ServiceStatus.menuMode = 0;
	switch (ServiceStatus.menuMode) {
		case ENCMode_LR:
			HDSP_LoadMiniHex((TIM4 ->CNT) << 16 | TIM3 ->CNT);
			break;
		case ENCMode_L:
			HDSP_LoadMiniHex(TIM4 ->CNT + (0xFFFF - TIM3 ->CNT));
			break;
		case ENCMode_R:
			HDSP_LoadMiniHex((0xFFFF-TIM4 ->CNT) + TIM3 ->CNT);
			break;
	}
}

void Service_CLBMode(){
	HDSP_LoadStr("CLBR",HDSP_ASCII);
	if (ServiceStatus.menuMode > 6)
		ServiceStatus.menuMode = 0;
	int encDiff=(int)((long)TIM3->CNT-(long)oldEnc+(((long)TIM3->CNT-(long)oldEnc<-50000)?65536:0)-(((long)TIM3->CNT-(long)oldEnc>50000)?65536:0))>>2;
	switch (ServiceStatus.menuMode) {
		case 0:
			HDSP_LoadStr("EXIT",HDSP_ASCII);
			break;
		case 1:
			if(toggleFlag){
				param[0]=Machine::getInstance().getTHF();
			}
			param[0]+=encDiff;
			HDSP_LoadInt(param[0]);
			break;
		case 2:
			if(toggleFlag){
				param[1]=Machine::getInstance().getTHS();
			}
			param[1]+=encDiff;
			HDSP_LoadInt(param[1]);
			break;
		case 3:
			if(toggleFlag){
				param[2]=Machine::getInstance().getOffsA1();
			}
			param[2]+=encDiff;
			HDSP_LoadInt(param[2]);
			break;
		case 4:
			if(toggleFlag){
				param[3]=Machine::getInstance().getOffsA2();
			}
			param[3]+=encDiff;
			HDSP_LoadInt(param[3]);
			break;
		case 5:
			if(toggleFlag){
				param[4]=Machine::getInstance().getOffsC1();
			}
			param[4]+=encDiff;
			HDSP_LoadInt(param[4]);
			break;
		case 6:
			if(toggleFlag){
				param[5]=Machine::getInstance().getOffsC2();
			}
			param[5]+=encDiff;
			HDSP_LoadInt(param[5]);
			break;
/*
		case 1:
			if(toggleFlag){
				param[6]=Machine::getInstance().getKAP()*100;
			}
			param[6]+=encDiff;
			HDSP_LoadInt(param[6]);
			break;
		case 2:
			if(toggleFlag){
				param[7]=Machine::getInstance().getKAI()*100;
			}
			param[7]+=encDiff;
			HDSP_LoadInt(param[7]);
			break;
		case 3:
			if(toggleFlag){
				param[8]=Machine::getInstance().getKAD()*100;
			}
			param[8]+=encDiff;
			HDSP_LoadInt(param[8]);
			break;
			*/
	}
	oldEnc=TIM3->CNT;
}

void Service_GYRMode(){
	if (ServiceStatus.menuMode > 2)
		ServiceStatus.menuMode = 0;
	if (!srvCounter) {
		long data=Machine::getInstance().getGZI();
		/*
		if (ServiceStatus.menuMode == GYRMode_X) {
			I2C_ReadGyroX(&data);
		} else if (ServiceStatus.menuMode == GYRMode_Y) {
			I2C_ReadGyroY(&data);
		} else {
			I2C_ReadGyroZ(&data);
		}
		*/
		HDSP_LoadMiniHex(data);
	}
	srvCounter++;
	srvCounter &= 0x3;
}

void Service_ADCMode(){
	if (ServiceStatus.menuMode > 4)
		ServiceStatus.menuMode = 0;
	if (!srvCounter) {
		uint32_t sum = 0;
		if(ServiceStatus.menuMode==ADCMode_All){
			const uint8_t place[4]={24,0,16,8};
			uint32_t word=0;
			for(int i=0;i<4;i++){
				sum=(ADC_ConvVal[i]+ADC_ConvVal[i+4])>>6;
				word|=(uint8_t)(((uint8_t)(sum/10)*0x10)|(sum%10))<<place[i];
			}
			HDSP_LoadMiniHex(word);
		} else {
			sum=(ADC_ConvVal[ServiceStatus.menuMode-1]+ADC_ConvVal[ServiceStatus.menuMode+3])>>6;
			HDSP_LoadInt(sum);
		}
	}
	srvCounter++;
	srvCounter &= 0x7;
}

void Service_BUZZERMode(){
	if (ServiceStatus.menuMode > 1)
		ServiceStatus.menuMode = 0;
	if (oldmenuSrvMode != ServiceStatus.menuMode) {
		if (ServiceStatus.menuMode == BUZMode_Enchukei+1) {
			HDSP_LoadStr("ENCH",HDSP_ASCII);
			Buzzer_PlayEnchukei();
		}
	}
	oldmenuSrvMode = ServiceStatus.menuMode;
}

void Service_MTRMode(){
	if (ServiceStatus.menuMode > 6)
		ServiceStatus.menuMode = 0;
	switch (ServiceStatus.menuMode) {
		case MTRMode_Stll:{
			if(!srvCounter){
				uint32_t sum=0;

				sum=(ADC_ConvVal[1]+ADC_ConvVal[5])>>6;
				if(oldMTRState==MTRState&&(sum > AD_Threshold)){
					/*
					if((ADC_ConvVal[2]+ADC_ConvVal[6])>>6 > AD_Threshold){
						ServiceStatus.menuMode++;
						MTRState=0;
						break;
					}
					*/
				}
				HDSP_LoadStr("STLL", HDSP_ASCII);
			}
			break;
		}
		case MTRMode_Rot: {
			if(!srvCounter){
				uint32_t sum=0;

				sum=(ADC_ConvVal[1]+ADC_ConvVal[5])>>6;
				if(oldMTRState==MTRState&&(sum > AD_Threshold)){
					/*
					if((ADC_ConvVal[2]+ADC_ConvVal[6])>>6 > AD_Threshold){
						ServiceStatus.menuMode++;
						MTRState=0;
						break;
					}
					*/
					MTRState++;
					targetAng=MTRState*45;
					EasingPoly3 *ep3=new EasingPoly3();
					MotionLinear *ml=new MotionLinear(ep3);
					Machine::getInstance().setDestinationInMilliMeter(Position(0, 0, targetAng), 30, ml,true);
				}
				if(sum<20){
					oldMTRState=MTRState;
				}
				int gzi=Machine::getInstance().getGZI();
				HDSP_LoadMiniHex(gzi);
			}
			break;
		}
		case MTRMode_StY: {
			if(!srvCounter){
				uint32_t sum=0;

				sum=(ADC_ConvVal[1]+ADC_ConvVal[5])>>6;
				if(oldMTRState==MTRState&&(sum > AD_Threshold)){
					/*
					if((ADC_ConvVal[2]+ADC_ConvVal[6])>>6 > AD_Threshold){
						ServiceStatus.menuMode++;
						MTRState=0;
						break;
					}
					*/
					MTRState++;
					targetY=MTRState*200;
					Machine::getInstance().setDestinationInMilliMeter(Position(0, -targetY, 0), 20, new MotionLinear(new EasingPoly3()),true);
				}
				if(sum<20){
					oldMTRState=MTRState;
				}
				char str[4];
				str[0]='S',str[1]='t',str[2]='Y';
				str[3]='0'+MTRState;
				HDSP_LoadStr(str,HDSP_ASCII);
			}
			break;
		}
		case MTRMode_Rec: {
			if(!srvCounter){
				uint32_t sum=0;

				sum=(ADC_ConvVal[1]+ADC_ConvVal[5])>>6;
				if(oldMTRState==MTRState&&(sum > AD_Threshold)){
					/*
					if((ADC_ConvVal[2]+ADC_ConvVal[6])>>6 > AD_Threshold){
						ServiceStatus.menuMode++;
						MTRState=0;
						break;
					}
					*/
					MTRState++;
					if(MTRState%2==0){
						targetAng-=90;
						Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), 50, new MotionLinear(new EasingPoly3()),true);
					}else if(MTRState%4==1){
						targetY=200-targetY;
						Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), 200, new MotionLinear(new EasingPoly3()),true);
					}else{
						targetX=200-targetX;
						Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), 200, new MotionLinear(new EasingPoly3()),true);
					}
				}
				if(sum<20){
					oldMTRState=MTRState;
				}
				char str[4];
				str[0]='R',str[1]='e',str[2]='c';
				str[3]='0'+MTRState;
				HDSP_LoadStr(str,HDSP_ASCII);
			}
			break;
		}
		case MTRMode_Auto: {
			if(!srvCounter){
				if(MTRState==0){
					uint32_t sum=0;

					sum=(ADC_ConvVal[1]+ADC_ConvVal[5])>>6;
					if(sum > AD_Threshold){
						MTRState=1;
						AutoFlag=true;
						/*
						Machine::getInstance().setDestinationInMilliMeter(
								Position(0, Maze_Block-70, 0),
								60,
								new MotionLinear(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(70, Maze_Block, -90),
								80,
								new MotionArc(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block-70, Maze_Block, -90),
								60,
								new MotionLinear(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block, Maze_Block-70, -180),
								80,
								new MotionArc(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block, 70, -180),
								60,
								new MotionLinear(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block+70, 0, -90),
								80,
								new MotionArc(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*2-70, 0, -90),
								60,
								new MotionLinear(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*2, 70, 0),
								80,
								new MotionArc(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*2, Maze_Block-70, 0),
								60,
								new MotionLinear(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*2+70, Maze_Block, -90),
								80,
								new MotionArc(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*4-70, Maze_Block, -90),
								120,
								new MotionLinear(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*4, Maze_Block-70, -180),
								80,
								new MotionArc(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*4, 70, -180),
								60,
								new MotionLinear(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*4-70, 0, -270),
								80,
								new MotionArc(new EasingLinear()));
						Machine::getInstance().setDestinationInMilliMeter(
								Position(Maze_Block*3, 0, -270),
								60,
								new MotionLinear(new EasingLinear()));
						*/
					}
					HDSP_LoadStr("AUTO",HDSP_ASCII);

				} else {
					/*
					Coord c=mouse->getCoord();
					uint8_t x=c.x;
					uint8_t y=c.y;
					HDSP_LoadInt(x*100+y);
					*/
					//HDSP_LoadMiniHex(Machine::getInstance().getEY());
				}

					/*
				}else if(Machine::getInstance().isTargetQueueEmpty()){
#define SLALOM_R 70
#define SLALOM_UNIT_TIME 120
#define SLALOM_STR_TIME ((float)SLALOM_UNIT_TIME*(200.f-2.f*SLALOM_R)/(200.f-0.5f*(float)SLALOM_R))
					switch(MTRState%8){
						case 0:
							targetAng-=90;
							targetY=SLALOM_R;
							targetX=0;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_UNIT_TIME-SLALOM_STR_TIME, new MotionArc(new EasingLinear()));
							break;
						case 1:
							targetY=200-SLALOM_R;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_STR_TIME, new MotionLinear(new EasingLinear()));
							break;
						case 2:
							targetAng-=90;
							targetX=SLALOM_R;
							targetY=200;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_UNIT_TIME-SLALOM_STR_TIME, new MotionArc(new EasingLinear()));
							break;
						case 3:
							targetX=200-SLALOM_R;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_STR_TIME, new MotionLinear(new EasingLinear()));
							break;
						case 4:
							targetAng-=90;
							targetX=200;
							targetY=200-SLALOM_R;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_UNIT_TIME-SLALOM_STR_TIME, new MotionArc(new EasingLinear()));
							break;
						case 5:
							targetY=SLALOM_R;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_STR_TIME, new MotionLinear(new EasingLinear()));
							break;
						case 6:
							targetAng-=90;
							targetX=200-SLALOM_R;
							targetY=0;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_UNIT_TIME-SLALOM_STR_TIME, new MotionArc(new EasingLinear()));
							break;
						case 7:
							targetX=SLALOM_R;
							Machine::getInstance().setDestinationInMilliMeter(Position(targetX, targetY, targetAng), SLALOM_STR_TIME, new MotionLinear(new EasingLinear()));
							break;
					}
					MTRState++;
					HDSP_LoadInt(Machine::getInstance().getEA());
				} else {
					HDSP_LoadInt(Machine::getInstance().getEA());
				}
				*/
			}
			break;
		}
		case MTRMode_Clk: {
			int encDiff=(int)((long)TIM3->CNT-(long)oldEnc+(((long)TIM3->CNT-(long)oldEnc<-50000)?65536:0)-(((long)TIM3->CNT-(long)oldEnc>50000)?65536:0))>>2;
			oldEnc=TIM3->CNT;
			if(MTRState==0){
				Machine::getInstance().deactivate();
				/* clock adjust */
				fivemsCounter=0;
				if(minCounter+encDiff>=0){
					minCounter+=encDiff;
				} else {
					if(hourCounter!=0){
						hourCounter--;
					} else {
						hourCounter=23;
					}
					minCounter=60+encDiff+minCounter;
				}
			} else {
				fivemsCounter++;
			}
			while(fivemsCounter>=200){
				secCounter++;
				fivemsCounter-=200;
			}
			while(secCounter>=60){
				minCounter++;
				secCounter-=60;
			}
			while(minCounter>=60){
				hourCounter++;
				minCounter-=60;
			}
			while(hourCounter>=24){
				hourCounter-=24;
			}
			if(fivemsCounter==0){
				HDSP_LoadInt(hourCounter*100+minCounter);
				if(MTRState){
					Machine::getInstance().setDestinationInMilliMeter(Position(0,0,-6*secCounter),80,new MotionLinear(new EasingPoly3()),false);
					if(secCounter==0){
						Buzzer_Add(880, 30);
						Buzzer_Add(1760, 30);
						Buzzer_Add(3520, 30);
					} else if(secCounter==30){
						Buzzer_Add(1319, 5);
					} else {
						Buzzer_Add(880, 5);
					}
				}
			}
			if(fivemsCounter>=100){
				HDSP_LoadInt(secCounter*100+(fivemsCounter/2));
			}
			if(!srvCounter){
				if(MTRState==0){
					uint32_t sum=0;
					sum=(ADC_ConvVal[1]+ADC_ConvVal[5])>>6;
					if(sum > AD_Threshold){
						fivemsCounter=0;
						Buzzer_Add(880, 30);
						Buzzer_Add(1760, 30);
						Buzzer_Add(3520, 30);
						Machine::getInstance().activate();
						Machine::getInstance().calibrateSensors();
						Machine::getInstance().initializeVariables();
						MTRState=1;
						TIM_Cmd(TIM5,DISABLE);
					}
				}
			}
		}
		case MTRMode_Cir: {
			if(!srvCounter){
				if(MTRState==0){
					uint32_t sum=0;
					sum=(ADC_ConvVal[1]+ADC_ConvVal[5])>>6;
					if(sum > AD_Threshold){
						Machine::getInstance().activate();
						Machine::getInstance().calibrateSensors();
						Machine::getInstance().setWallCtrl(true);
						MTRState=1;
						Route route;
						for(int i=0;i<10;i++){
							route.push_back(240);
							route.push_back(240-16*1);
							route.push_back(240-16*2);
							route.push_back(240-16*3);
							route.push_back(240-16*4);
							route.push_back(240-16*5);
							route.push_back(240-16*6);
							route.push_back(240-16*7);
							route.push_back(240-16*7+1);
							route.push_back(240-16*6+1);
							route.push_back(240-16*5+1);
							route.push_back(240-16*4+1);
							route.push_back(240-16*3+1);
							route.push_back(240-16*2+1);
							route.push_back(240-16*1+1);
							route.push_back(240+1);
						}
						mouse->generateFastestPath(route);
					}
				} else {
					if(Machine::getInstance().isTargetQueueEmpty()){
						Machine::getInstance().setWallCtrl(false);
					}
				}
				char str[4];
				str[0]='C',str[1]='i',str[2]='r',str[3]='c';
			}
			break;
		}
	}
	srvCounter++;
	srvCounter &= 0x7;
}

void Service_STGMode(){
	if (ServiceStatus.menuMode > 3)
		ServiceStatus.menuMode = 0;
	if (ServiceStatus.menuMode != STGMode_Retn) {
		if (toggleFlag) {
			ServiceStatus.srvOpt ^= (1 << (ServiceStatus.menuMode - 1));
		}
		char str[5];
		strcpy(str, settingText[ServiceStatus.menuMode - 1]);
		str[3] = (ServiceStatus.srvOpt & (1 << (ServiceStatus.menuMode - 1))) ? 'E' : 'D';
		HDSP_LoadStr(str, HDSP_ASCII);
	} else {
		HDSP_LoadStr("EXIT", HDSP_ASCII);
	}
}
