/*
 *
 * usart.h
 *
 *  Created on: 2013/02/20
 *      Author: yuichi
 */

#ifndef USART_H_
#define USART_H_

#include "stm32f4xx.h"
#ifdef __cplusplus
extern "C" {
#endif
#define USART_NBUF 32

void USART_DMAConfig();
void USART_Config();
void USART_Gets(char *buf);
void USART_Putc(uint8_t byte);
void USART_Puts(const char *str);
void USART_WriteByteByHex(uint8_t byte);
void USART_WriteByteByDigit(uint8_t byte);
void USART_WriteHalfWordByHex(uint16_t hword);
void USART_WriteHalfWordByDigit(uint16_t hword);
void USART_WriteWordByHex(uint32_t word);
void USART_WriteWordByDigit(uint32_t word);
void USART_WriteNewLine(void);
uint8_t USART_GetChrCmd();
void USART_PushTxBuffer();
int USART_scanf(const char* format, int narg, ...);
void USART_printf(const char* format, int narg, ...);

inline void VCP_Putc(char ch){
	return USART_Putc(ch);
}
inline void VCP_Puts(const char *str){
	return USART_Puts(str);
}
#ifdef __cplusplus
}
#endif
#endif /* USART_H_ */
