/*
 * functors.h
 *
 *  Created on: 2013/10/25
 *      Author: yuichi
 */

#ifndef FUNCTORS_H_
#define FUNCTORS_H_

#include "position.h"
#include "usart.h"

class EasingFunctor{
protected:
	float time;
public:
	EasingFunctor(){}
	EasingFunctor(float _time):time(_time){}
	EasingFunctor(const EasingFunctor& obj){
		time=obj.time;
	}
	virtual ~EasingFunctor();
	virtual float func(unsigned int s){};
	virtual void initialize(){}
	void setTime(float _time){ time=_time; }
	float getTime() const { return time; }
};

class MotionFunctor{
protected:
	Position* origin;
	Position* dest;
	float time;
	EasingFunctor *e;
public:
	MotionFunctor(){}
	MotionFunctor(Position _origin,Position _dest,float _time,EasingFunctor *_e){
		origin=new Position(_origin);
		dest=new Position(_dest);
		time=_time;
		e=_e;
	}
	MotionFunctor(EasingFunctor *_e){
		e=_e;
	}
	MotionFunctor(const MotionFunctor& obj){
		origin=obj.origin;
		dest=obj.dest;
		time=obj.time;
		e=obj.e;
	}
	virtual ~MotionFunctor();
	void destroy(){ delete origin; delete dest; delete e; }
	Position getOrigin() const { return *origin; }
	Position getDest() const { return *dest; }
	EasingFunctor *getE() const { return e; }
	void setParams(Position _origin,Position _dest,float _time){
		origin=new Position(_origin);
		dest=new Position(_dest);
		time=_time;
		e->setTime(time);
		initialize();
		e->initialize();
	}
	virtual Position func(unsigned int s){}
	virtual void initialize(){}
};

#endif /* FUNCTORS_H_ */
