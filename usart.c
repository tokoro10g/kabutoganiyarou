/*
 * usart.c
 *
 *  Created on: 2013/02/20
 *      Author: yuichi
 */

#include "usart.h"
#include "Baselibc/stdio.h"
#include "Baselibc/string.h"
#include "stdarg.h"
#include "va_narg.h"

volatile uint8_t USART_TxBuffer[32][128];
volatile uint8_t USART_TxBufLinePos = 0;
volatile uint8_t USART_TxReadLinePos = 0;
volatile uint8_t USART_TxWaitFlag = 1;

volatile uint8_t USART_RxBuffer[32][64];
volatile uint8_t USART_RxBufLinePos = 0;
volatile uint8_t USART_RxReadLinePos = 0;
volatile uint8_t USART_RxBufPos = 0;
volatile uint8_t USART_RxReadPos = 0;
volatile uint8_t USART_RxWaitFlag = 0;

void USART_DMAConfig() {
	DMA_InitTypeDef DMA_InitStructure;

	DMA_DeInit(DMA1_Stream3 );
	DMA_InitStructure.DMA_Channel = DMA_Channel_4;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t) &USART3 ->DR;
	DMA_InitStructure.DMA_Memory0BaseAddr =
			(uint32_t) USART_TxBuffer[USART_TxReadLinePos];
	DMA_InitStructure.DMA_DIR = DMA_DIR_MemoryToPeripheral;
	DMA_InitStructure.DMA_BufferSize = strlen(
			USART_TxBuffer[USART_TxReadLinePos]);
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_VeryHigh;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	while (DMA_GetCmdStatus(DMA1_Stream3 ) != DISABLE)
		;
	DMA_Init(DMA1_Stream3, &DMA_InitStructure);
	USART_DMACmd(USART3, USART_DMAReq_Tx, ENABLE);
	DMA_ITConfig(DMA1_Stream3, DMA_IT_TC | DMA_IT_TE, ENABLE);
	DMA_Cmd(DMA1_Stream3, ENABLE);
}

void USART_Config() {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource10, GPIO_AF_USART3 );

	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOC, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOC, GPIO_PinSource11, GPIO_AF_USART3 );

	USART_InitTypeDef USART_InitStruct;
	USART_StructInit(&USART_InitStruct);
	USART_InitStruct.USART_BaudRate = 921600;
	USART_InitStruct.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;
	USART_Init(USART3, &USART_InitStruct);

	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = USART3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x02;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x03;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = DMA1_Stream3_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x03;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
	USART_Cmd(USART3, ENABLE);
}

void USART_Putc(uint8_t byte) {
	while (USART_GetFlagStatus(USART3, USART_FLAG_TXE ) == RESET)
		;
	USART_SendData(USART3, byte);
}

void USART_Puts(const char *str) {
	strcpy(USART_TxBuffer[USART_TxBufLinePos], str);
	if (USART_TxWaitFlag) {
		USART_TxWaitFlag=0;
		while (USART_GetFlagStatus(USART3, USART_FLAG_TC ) == RESET)
			;
		USART_DMAConfig();
		USART_TxReadLinePos++;
		USART_TxReadLinePos &= 31;
	}
	USART_TxBufLinePos++;
	USART_TxBufLinePos &= 31;
}

void USART_Gets(char *buf) {
	while (strchr(USART_RxBuffer[USART_RxReadLinePos], '\r') == 0)
		;
	strcpy(buf, USART_RxBuffer[USART_RxReadLinePos]);
	for (int i = 0; i < 64; i++) {
		USART_RxBuffer[USART_RxReadLinePos][i] = 0;
	}
	USART_RxReadLinePos++;
	USART_RxReadLinePos &= 31;
}

void USART_WriteByteByHex(uint8_t byte) {
	USART_Putc(
			((byte & 0xF0) >> 4)
					+ (((byte & 0xF0) < 0xA0) ? '0' : ('A' - 0xA)));
	USART_Putc((byte & 0x0F) + (((byte & 0x0F) < 0x0A) ? '0' : ('A' - 0xA)));
}

void USART_WriteByteByDigit(uint8_t byte) {
	if (!byte)
		USART_Putc('0');
	uint8_t base = 100;
	uint8_t digit;
	while (base != 0) {
		digit = byte / base;
		byte -= digit * base;
		base /= 10;
		USART_Putc(digit + '0');
	}
}

void USART_WriteHalfWordByHex(uint16_t hword) {
	USART_WriteByteByHex((hword & 0xFF00) >> 8);
	USART_WriteByteByHex(hword & 0x00FF);
}

void USART_WriteHalfWordByDigit(uint16_t hword) {
	if (!hword)
		USART_Putc('0');
	uint16_t base = 10000;
	uint8_t digit;
	while (hword < base)
		base /= 10;
	while (base != 0) {
		digit = hword / base;
		hword -= digit * base;
		base /= 10;
		USART_Putc(digit + '0');
	}
}

void USART_WriteWordByHex(uint32_t word) {
	USART_WriteByteByHex((word & 0xFF000000) >> 24);
	USART_WriteByteByHex((word & 0x00FF0000) >> 16);
	USART_WriteByteByHex((word & 0x0000FF00) >> 8);
	USART_WriteByteByHex(word & 0x000000FF);
}

void USART_WriteWordByDigit(uint32_t word) {
	if (!word)
		USART_Putc('0');
	uint32_t base = 1000000000;
	uint8_t digit;
	while (word < base)
		base /= 10;
	while (base != 0) {
		digit = word / base;
		word -= digit * base;
		base /= 10;
		USART_Putc(digit + '0');
	}
}

void USART_WriteNewLine(void) {
	USART_Puts("\r\n");
}

uint8_t USART_GetChrCmd() {
	USART_Puts("Enter command >");
	char ch[64];
	USART_Gets(ch);
	if (strlen(ch) > 3) {
		USART_Puts("\r\n[Error]Invalid Command.\r\n");
		return 0;
	}
	return ch[0];
}

void USART_PushTxBuffer() {
	if (USART_TxBufLinePos != USART_TxReadLinePos) {
		while (USART_GetFlagStatus(USART3, USART_FLAG_TXE ) == RESET)
			;
		USART_DMAConfig();
		USART_TxReadLinePos++;
		USART_TxReadLinePos &= 31;
	} else {
		//USART_DMACmd(USART3,USART_DMAReq_Tx,DISABLE);
		DMA_ITConfig(DMA1_Stream3, DMA_IT_TC, DISABLE);
		USART_TxWaitFlag=1;
	}
}

int USART_scanf(const char* format, int narg, ...){
	int status=0;
	char strbuf[128];
	va_list vl;
	va_start(vl,narg);
	USART_Gets(strbuf);
	if (vsscanf(strbuf,format,vl)!=narg) {
		USART_Puts("Invalid value.\r\n");
		status=-1;
	}
	va_end(vl);
	return status;
}

void USART_printf(const char* format, int narg, ...){
	char strbuf[128];
	va_list vl;
	va_start(vl,narg);
	vsprintf(strbuf,format,vl);
	USART_Puts(strbuf);
	va_end(vl);
}
