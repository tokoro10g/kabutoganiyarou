/*
 * eeptools.cpp
 *
 *  Created on: 2013/07/02
 *      Author: yuichi
 */

#include "stm32f4xx_conf.h"
#include "eeptools.h"
#include "eeprom.h"
#include "usart.h"

typedef union {
	float fval;
	uint32_t uval;
} FU32;

uint16_t VirtAddVarTab[NB_OF_VAR ];

void EEP_Init() {
	for (int i = 0; i < NB_OF_VAR ; i++) {
		VirtAddVarTab[i] = i;
	}
	FLASH_Unlock();
	EE_Init();
}

void EEP_Write(uint16_t address, uint16_t data) {
	EE_WriteVariable(address, data);
}

void EEP_WriteFloat(uint16_t address0, float data) {
	FU32 fu;
	fu.fval = data;
	/*
	FLASH_Unlock();
	FLASH_ClearFlag(
			FLASH_FLAG_EOP | FLASH_FLAG_BSY | FLASH_FLAG_PGAERR
					| FLASH_FLAG_OPERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR
					| FLASH_FLAG_WRPERR );
	if (FLASH_ProgramWord(0x08004000, fu.uval) != FLASH_COMPLETE) {
		USART_Puts("[err] Flash write failed.");
	}
	*/
	EE_WriteVariable(VirtAddVarTab[address0],(fu.uval&0xFFFF0000)>>16);
	EE_WriteVariable(VirtAddVarTab[address0+1],fu.uval&0xFFFF);
}

uint16_t EEP_Read(uint16_t address, uint16_t *data) {
	return EE_ReadVariable(address, data);
}

float EEP_ReadFloat(uint16_t address0, float *data) {
	FU32 fu;
	uint16_t tmph,tmpl;
	/*
	uint32_t val = *(__IO uint32_t *) (0x08004000);
	fu.uval = val;
	*/
	EE_ReadVariable(address0,&tmph);
	EE_ReadVariable(address0+1,&tmpl);
	fu.uval=((uint32_t)tmph)<<16|(uint32_t)tmpl;
	*(data) = fu.fval;
	return fu.fval;
}

void EEP_Erase() {
	FLASH_Unlock();
	FLASH_ClearFlag(
			FLASH_FLAG_EOP | FLASH_FLAG_BSY | FLASH_FLAG_PGAERR
					| FLASH_FLAG_OPERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR
					| FLASH_FLAG_WRPERR );
	FLASH_EraseSector(FLASH_Sector_1, VoltageRange_3 );
	FLASH_EraseSector(FLASH_Sector_2, VoltageRange_3 );
}
