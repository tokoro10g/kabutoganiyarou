/*
 * machine.cpp
 *
 *  Created on: 2013/06/19
 *      Author: yuichi
 */

#include "machine.h"
#include "motor.h"
#include "mymath.h"

#include "math.h"

#include "adc.h"
#include "usart.h"
#include "i2c.h"
#include "Logger.h"

#include "userfunctors.h"

#include "hdsp2000lp.h"

extern bool AutoFlag;

void Machine::MachineService(){
	if(!activeflag) return;

	if(!isTargetQueueEmpty()){
		time++;
		Position p=targetQueue.front()->getCurrentTarget(time);
		tx=p.x;
		ty=p.y;
		tangle=p.angle;
		setWallCtrl(targetQueue.front()->isWallCtrlActive());
		USART_printf("%d\t%d\r\n",2,tx,ty);
	}

	int vl=motorL.getV();
	int vr=motorR.getV();
	v=vl+vr;

	int rr=vr,rl=vl;
	omega=vr-vl;
	int fl=ADC_ConvVal[0]+ADC_ConvVal[4];
	int fr=ADC_ConvVal[1]+ADC_ConvVal[5];
	int l=ADC_ConvVal[2]+ADC_ConvVal[6];
	int r=ADC_ConvVal[3]+ADC_ConvVal[7];

	if(wallCtrlFlag&&!isTargetQueueEmpty()){
		if(l-r>Th_s){
			long diffl=l-adcDefVal[2];
			gz_i+=diffl/Offs_a1;
			x-= +(int)((float)(diffl)/Offs_c1*mycos(ANGLE_CONST*(float)tangle));
			y-= +(int)((float)(diffl)/Offs_c1*mysin(ANGLE_CONST*(float)tangle));
		} else if(r-l>Th_s){
			long diffr=r-adcDefVal[3];
			gz_i-=diffr/Offs_a1;
			x-= -(int)((float)(diffr)/Offs_c1*mycos(ANGLE_CONST*(float)tangle));
			y-= -(int)((float)(diffr)/Offs_c1*mysin(ANGLE_CONST*(float)tangle));
		}
		if(fl+fr<Th_f){
			// there is no wall in front of the machine
			gz_i+=(fl-fr)/Offs_a2;

			if(l>Th_s&&r>Th_s){
				x-= +(int)((float)(l-r)/Offs_c2*mycos(ANGLE_CONST*(float)tangle));
				y-= +(int)((float)(l-r)/Offs_c2*mysin(ANGLE_CONST*(float)tangle));
				gz_i+=(l-r)/256;
			}
		}
	}

	long diffa=(long)(CGYRO_ANGLE_CONST*(float)gz_i)-angle;
	long erra=diffa-omega;
	v-=erra/2;
	//angle+=(long)((float)omega*0.3f+(float)diffa*0.7f);

	angle=(long)(CGYRO_ANGLE_CONST*(float)gz_i);

	x-=(int)((float)v/2.f*sin(ANGLE_CONST*(float)angle));
	y+=(int)((float)v/2.f*cos(ANGLE_CONST*(float)angle));

	long ex=tx-x,ey=ty-y;
	long ea=tangle-angle;
	//long eag=tangle-(CGYRO_ANGLE_CONST*gz_i);


	float theta=-myatan2((float)ex,(float)ey)*CANGLE_CONST;
	rangle=theta-(float)angle;
	while(rangle>PI*CANGLE_CONST){
		rangle-=2.f*PI*CANGLE_CONST;
	}
	while(rangle<-PI*CANGLE_CONST){
		rangle+=2.f*PI*CANGLE_CONST;
	}
	float ranglebase=rangle;
	while(rangle>PI*CANGLE_CONST/2.f){
		rangle=-rangle+PI*CANGLE_CONST;
	}
	while(rangle<-PI*CANGLE_CONST/2.f){
		rangle=-rangle-PI*CANGLE_CONST;
	}

	e=(long)myhypot((float)ex,(float)ey);

	if(ABS(ex)<50) ex=0;
	if(ABS(ey)<50) ey=0;
	//if(ABS(ea)<50) ea=0;

	long e_d=e-olde;
	long a_d=ea-oldea;
	float ra_d=rangle-oldrangle;

	a_i=(long)(0.98f*(float)a_i)+(ea+oldea)/2;
	e_i=(long)(0.97f*(float)e_i)+(e+olde)/2;

	olda=angle;
	olde=e;
	oldea=ea;
	oldgzi=gz_i;
	oldrangle=rangle;

	float rv=(ABS(ranglebase)>PI*CANGLE_CONST/2.f?-1.f:1.f)*ABS(mycos(ranglebase*ANGLE_CONST))*(Kp_p*(float)e+Kp_i*(float)e_i+Kp_d*(float)e_d);
	float romega=(Ka_p*(float)ea+Ka_i*(float)a_i+Ka_d*(float)a_d)/(0.00025f*(float)ABS(e)+1.f)+(Kra_p*rangle+Kra_d*ra_d)*(0.00025f*(float)ABS(e)/(0.000025f*(float)ABS(e)+1.f));
	//float romega=(Ka_p*(float)ea+Ka_i*(float)a_i+Ka_d*(float)a_d);//+Ka_p*(ralpha);

	if(wallCtrlFlag&&fl+fr>Th_f*2&&!isTargetQueueEmpty()){
		rv*=(float)Th_f*2.f/(fl+fr);
	}

	rr=(int)(rv/2.f); rl=(int)(rv/2.f);
	rr+=(int)romega; rl-=(int)romega;

	if(isLogging){
		xLogger->enqueue(x);
		yLogger->enqueue(y);
		angleLogger->enqueue(angle);
		txLogger->enqueue(tx);
		tyLogger->enqueue(ty);
		eaLogger->enqueue(ea);
		aiLogger->enqueue(a_i);
		rvLogger->enqueue(rv);
		romegaLogger->enqueue(romega);
		rangleLogger->enqueue(rangle);
	}

	if(ABS(rr)>320000||ABS(rl)>320000){
		AutoFlag=false;
		throw -1;
	}

	setMotorMillimeterPerSec((int)(MOTOR_CONST*(float)rl),(int)(MOTOR_CONST*(float)rr));
	motorL.MotorService();
	motorR.MotorService();

	if(!isTargetQueueEmpty() && time==(targetQueue.front()->getTime())){
		time=0;
		targetQueue.front()->destroy();
		delete targetQueue.front();
		targetQueue.pop();
		setWallCtrl(false);
	}
}

void Machine::setDestinationInMilliMeter(long _x,long _y,int _angle,unsigned int _time){
	long px=(long)(CMOTOR_CONST*(float)_x);
	long py=(long)(CMOTOR_CONST*(float)_y);
	long pangle=(long)((float)_angle*CANGLE_CONST_DEG);
	setDestination(px,py,pangle,_time);
}

void Machine::setDestinationInMilliMeter(Position dest,unsigned int _time,MotionFunctor *mf,bool isWallCtrlActive){
	long px=(long)(CMOTOR_CONST*(float)dest.x);
	long py=(long)(CMOTOR_CONST*(float)dest.y);
	long pangle=dest.angle;
	while((float)fdest.angle*ANGLE_CONST_DEG-pangle>180){
		pangle+=360;
	}
	while((float)fdest.angle*ANGLE_CONST_DEG-pangle<-180){
		pangle-=360;
	}
	pangle=(long)((float)pangle*CANGLE_CONST_DEG);
	USART_printf("target:(%d,%d,%d)\r\n",3,dest.x,dest.y,dest.angle);
	setDestination(Position(px,py,pangle),_time,mf,isWallCtrlActive);
}

void Machine::setDestination(long _x,long _y,long _angle,unsigned int _time){
	Position dest=Position(_x,_y,_angle);
	EasingPoly3 *ep3=new EasingPoly3(_time);
	MotionLinear *ml=new MotionLinear(fdest,dest,_time,ep3);
	Target *t=new Target(fdest,dest,_time,ml,true);
	targetQueue.push(t);
	fdest.setVars(_x,_y,_angle);
}

void Machine::setDestination(Position dest,unsigned int _time,MotionFunctor *mf,bool isWallCtrlActive){
	Target *t=new Target(fdest,dest,_time,mf,isWallCtrlActive);
	targetQueue.push(t);
	fdest.setVars(dest.x,dest.y,dest.angle);
}

void Machine::setMotorMillimeterPerSec(int l,int r){
	motorL.setMilliMeterPerSec(l);
	motorR.setMilliMeterPerSec(r);
}

void Machine::setMotorPWMOffset(uint8_t _offset){
	motorL.setMotorOffset(_offset);
	motorR.setMotorOffset(_offset);
}

void Machine::setMachinePID(float _K_p,float _K_i,float _K_d){
	if(myfabs(_K_p)>500) _K_p=0;
	if(myfabs(_K_i)>500) _K_i=0;
	if(myfabs(_K_d)>500) _K_d=0;
	Kp_p=_K_p;
	Kp_i=_K_i;
	Kp_d=_K_d;
}

void Machine::setAnglePID(float _K_p,float _K_i,float _K_d){
	if(myfabs(_K_p)>500) _K_p=0;
	if(myfabs(_K_i)>500) _K_i=0;
	if(myfabs(_K_d)>500) _K_d=0;
	Ka_p=_K_p;
	Ka_i=_K_i;
	Ka_d=_K_d;
}

void Machine::setRAnglePD(float _K_p,float _K_d){
	if(myfabs(_K_p)>500) _K_p=0;
	if(myfabs(_K_d)>500) _K_d=0;
	Kra_p=_K_p;
	Kra_d=_K_d;
}

void Machine::setMotorPID(float _K_p,float _K_i,float _K_d){
	if(myfabs(_K_p)>500) _K_p=0;
	if(myfabs(_K_i)>500) _K_i=0;
	if(myfabs(_K_d)>500) _K_d=0;
	motorL.setMotorPID(_K_p,_K_i,_K_d);
	motorR.setMotorPID(_K_p,_K_i,_K_d);
}

void Machine::activate(){
	if(!activeflag){
		initializeVariables();
		motorL.initialize();
		motorR.initialize();
		activeflag=true;
	}
}

void Machine::deactivate(){
	if(activeflag){
		motorL.eStop();
		motorR.eStop();
		activeflag=false;
	}
}

void Machine::calibrateSensors(){
	for(int i=0;i<3;i++){
		long sum=0;
		for(int j=0;j<1024;j++){
			int val;
			I2C_ReadAccBase(0x28+2*i,&val);
			sum+=val;
		}
		sum=(sum+512)/1024;
		accDefVal[i]=sum;
	}
	USART_printf("AccCalib:%d,%d,%d\r\n",3,accDefVal[0],accDefVal[1],accDefVal[2]);
	for(int i=0;i<3;i++){
		long sum=0;
		for(int j=0;j<1024;j++){
			int val;
			I2C_ReadGyroBase(0x28+2*i,&val);
			sum+=val;
		}
		sum=(sum+512)/1024;
		gyroDefVal[i]=sum;
	}
	USART_printf("GyrCalib:%d,%d,%d\r\n",3,gyroDefVal[0],gyroDefVal[1],gyroDefVal[2]);
	adcDefVal[0]=(ADC_ConvVal[0]+ADC_ConvVal[4]);
	adcDefVal[1]=(ADC_ConvVal[1]+ADC_ConvVal[5]);
	adcDefVal[2]=(ADC_ConvVal[2]+ADC_ConvVal[6]);
	adcDefVal[3]=(ADC_ConvVal[3]+ADC_ConvVal[7]);
}

void Machine::readSensors(){
	if(I2C_ReadAccX(&ax)){ ax=oldax; }
	ax=(ax-accDefVal[0]+16)>>5;
	ax_i+=oldax+ax;
	oldax=ax;
	if(I2C_ReadAccY(&ay)){ ay=olday; }
	ay=(ay-accDefVal[1]+16)>>5;
	ay_i+=olday+ay;
	olday=ay;
	if(I2C_ReadAccZ(&az)){ az=oldaz; }
	az=(az-accDefVal[2]+16)>>5;
	az_i+=oldaz+az;
	oldaz=az;
	if(I2C_ReadGyroX(&gx)){ gx=oldgx; }
	gx=(gx-gyroDefVal[0]+16)>>5;
	gx_i+=oldgx+gx;
	oldgx=gx;
	if(I2C_ReadGyroY(&gy)){ gy=oldgy; }
	gy=(gy-gyroDefVal[1]+16)>>5;
	gy_i+=oldgy+gy;
	oldgy=gy;
	if(I2C_ReadGyroZ(&gz)){ gz=oldgz; }
	gz=(gz-gyroDefVal[2]+16)>>5;
	gz_i+=oldgz+gz;
	oldgz=gz;
}

void Machine::printAllLog() const {
	int size=xLogger->getSize();
	USART_Puts("t\tx\ty\tangle\ttx\tty\tea\tai\trangle\trv\tromega\r\n");
	for(int i=0;i<size;i++){
		USART_printf(
				"%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\r\n",
				11,
				i,
				xLogger->dequeue(),
				yLogger->dequeue(),
				angleLogger->dequeue(),
				txLogger->dequeue(),
				tyLogger->dequeue(),
				eaLogger->dequeue(),
				aiLogger->dequeue(),
				rangleLogger->dequeue(),
				rvLogger->dequeue(),
				romegaLogger->dequeue()
		);
		for(volatile int j=0;j<12000;j++);
	}
}

void MachineReadSensors(){
	Machine::getInstance().readSensors();
}
