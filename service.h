/*
 * service.h
 *
 *  Created on: 2013/03/14
 *      Author: yuichi
 */

#ifndef SERVICE_H_
#define SERVICE_H_
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	unsigned srvMode :3;
	unsigned srvOpt :5;
	unsigned oldSrvMode :3;
	unsigned menuMode :3;
} SrvStat_t;

void ServiceHandler();
void incServiceMode();
void toggleMenu();
void Service_ENCMode();
void Service_CLBMode();
void Service_GYRMode();
void Service_ADCMode();
void Service_BUZZERMode();
void Service_MTRMode();
void Service_STGMode();

#ifdef __cplusplus
}
#endif
#endif /* SERVICE_H_ */
