/*
 * eeptools.h
 *
 *  Created on: 2013/07/02
 *      Author: yuichi
 */

#ifndef EEPTOOLS_H_
#define EEPTOOLS_H_

#include "stm32f4xx_conf.h"
#include "eeprom.h"

void EEP_Init();
void EEP_Write(uint16_t address,uint16_t data);
void EEP_WriteFloat(uint16_t address0,float data);
uint16_t EEP_Read(uint16_t address,uint16_t *data);
float EEP_ReadFloat(uint16_t address0,float *data);
void EEP_Erase();

#define PARAM_MOTOR_P 0
#define PARAM_MOTOR_I 2
#define PARAM_MOTOR_D 4
#define PARAM_POS_P 6
#define PARAM_POS_I 8
#define PARAM_POS_D 16
#define PARAM_ANG_P 10
#define PARAM_ANG_I 12
#define PARAM_ANG_D 14
#define PARAM_SRV_OPT 18
#define PARAM_MOTOR_OFFSET 19
#define PARAM_RANG_P 20
#define PARAM_RANG_D 22
#define PARAM_WALL_FRONT_THRESHOLD 24
#define PARAM_WALL_SIDE_THRESHOLD 26
#define PARAM_OFFSET_ANGLE_ONE_WALL 28
#define PARAM_OFFSET_ANGLE_TWO_WALLS 30
#define PARAM_OFFSET_COORD_ONE_WALL 32
#define PARAM_OFFSET_COORD_TWO_WALLS 34

#endif /* EEPTOOLS_H_ */
