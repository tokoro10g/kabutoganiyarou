/*
 * adc.h
 *
 *  Created on: 2013/07/17
 *      Author: yuichi
 */

#include "stm32f4xx.h"

#ifndef ADC_H_
#define ADC_H_

#ifdef __cplusplus
extern "C"{
#endif

void ADC_Config();
void ADC_Read();
void ADC_ReadAll();
void ADC_Enable();
void ADC_Disable();
extern volatile uint16_t ADC_ConvVal[8];
extern uint8_t ADC_Status;

#ifdef __cplusplus
}
#endif

#endif /* ADC_H_ */
