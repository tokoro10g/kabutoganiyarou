/*
 * i2c.h
 *
 *  Created on: 2013/02/13
 *      Author: yuichi
 */

#ifndef I2C_H_
#define I2C_H_

#include "stm32f4xx.h"
#ifdef __cplusplus
extern "C" {
#endif
#define I2Cx				I2C2
#define I2Cx_GPIO			GPIOB
#define I2Cx_RCC_IO			RCC_AHB1Periph_GPIOB
#define I2Cx_RCC			RCC_APB1Periph_I2C2
#define I2Cx_SCL			GPIO_Pin_10
#define I2Cx_SDA			GPIO_Pin_11
#define I2Cx_PinSource_SCL	GPIO_PinSource10
#define I2Cx_PinSource_SDA	GPIO_PinSource11
#define I2Cx_GPIO_AF		GPIO_AF_I2C2
#define I2Cx_Speed			400000

#define I2C_ADDRESS_ACCEL 0b00110010
#define I2C_ADDRESS_GYRO 0b11010010

extern uint8_t I2C_ErrFlag;

void I2C_Config();
uint8_t I2C_start(uint8_t address, uint8_t direction);
uint8_t I2C_write(uint8_t data);
uint8_t I2C_read_ack(uint8_t *ptr);
uint8_t I2C_read_nack(uint8_t *ptr);
void I2C_stop();
uint8_t I2C_WaitForBusy();

void I2C_AccCmd(uint8_t addr, uint8_t val);
void I2C_GyroCmd(uint8_t addr, uint8_t val);
uint8_t I2C_ReadAccBase(uint8_t baseaddr, int *ptr);
uint8_t I2C_ReadGyroBase(uint8_t baseaddr, int *ptr);

inline uint8_t I2C_ReadAccX(int *ptr) {
	return I2C_ReadAccBase(0x28,ptr);
}

inline int I2C_ReadAccY(int *ptr) {
	return I2C_ReadAccBase(0x2a,ptr);
}

inline int I2C_ReadAccZ(int *ptr) {
	return I2C_ReadAccBase(0x2c,ptr);
}

inline int I2C_ReadGyroX(int *ptr) {
	return I2C_ReadGyroBase(0x28,ptr);
}

inline int I2C_ReadGyroY(int *ptr) {
	return I2C_ReadGyroBase(0x2a,ptr);
}

inline int I2C_ReadGyroZ(int *ptr) {
	return I2C_ReadGyroBase(0x2c,ptr);
}

#ifdef __cplusplus
}
#endif
#endif /* I2C_H_ */
