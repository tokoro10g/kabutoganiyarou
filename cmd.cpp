/*
 * cmd.cpp
 *
 *  Created on: 2013/04/12
 *      Author: yuichi
 */

#include "stdint.h"
#include "button.h"
#include "usart.h"
#include "eeptools.h"
#include "eeprom.h"
#include "machine.h"
#include "service.h"
#include "Logger.h"

#include "mouse.hpp"
#include "maze.hpp"
#include "graph.hpp"

#include <Baselibc/stdio.h>

extern SrvStat_t ServiceStatus;
extern Mouse *mouse;

void Cmd_Proc() {
	uint8_t USARTCmd;
	USARTCmd= USART_GetChrCmd();
	switch (USARTCmd) {
		case '@': {
			float K_p, K_i, K_d;
			uint8_t cmd;
			USART_Puts("Change PID parameters\r\n");
			USART_Puts("(0)Motor (1)Machine_p (2)Machine_a (3)Machine_ra\r\n");
			USART_Puts("(4)Motor_o (5)Exit: ");
			if (USART_scanf("%u",1,&cmd)==-1) break;
			if (cmd >= 4) {
				USART_Puts("Abort.\r\n");
				break;
			}
			switch (cmd) {
				case 0: {
					EEP_ReadFloat(PARAM_MOTOR_P, &K_p);
					EEP_ReadFloat(PARAM_MOTOR_I, &K_i);
					EEP_ReadFloat(PARAM_MOTOR_D, &K_d);
					USART_printf("K_p:%f, K_i:%f, K_d:%f\r\n",3, K_p, K_i, K_d);
					USART_Puts("Change PID parameters for Motor Velocity\r\nK_p: ");
					if(USART_scanf("%f",1, &K_p)==-1) break;
					if(K_p<0) break;
					USART_Puts("K_i: ");
					if(USART_scanf("%f",1, &K_i)==-1) break;
					if(K_i<0) break;
					USART_Puts("K_d: ");
					if(USART_scanf("%f",1, &K_d)==-1) break;
					if(K_d<0) break;
					EEP_WriteFloat(PARAM_MOTOR_P, K_p);
					EEP_ReadFloat(PARAM_MOTOR_P, &K_p);
					EEP_WriteFloat(PARAM_MOTOR_I, K_i);
					EEP_ReadFloat(PARAM_MOTOR_I, &K_i);
					EEP_WriteFloat(PARAM_MOTOR_D, K_d);
					EEP_ReadFloat(PARAM_MOTOR_D, &K_d);
					USART_printf("PID parameters have been set to %f,%f,%f\r\n",3,K_p,K_i,K_d);
					Machine::getInstance().setMotorPID(K_p, K_i, K_d);
					break;
				}
				case 1:{
					EEP_ReadFloat(PARAM_POS_P, &K_p);
					EEP_ReadFloat(PARAM_POS_I, &K_i);
					EEP_ReadFloat(PARAM_POS_D, &K_d);
					USART_printf("K_p:%f, K_i:%f, K_ii:%f\r\n",3, K_p, K_i, K_d);
					USART_Puts("Change PID parameters for Machine Position\r\nK_p: ");
					if(USART_scanf("%f",1, &K_p)==-1) break;
					if(K_p<0) break;
					USART_Puts("K_i: ");
					if(USART_scanf("%f",1, &K_i)==-1) break;
					if(K_i<0) break;
					USART_Puts("K_d: ");
					if(USART_scanf("%f",1, &K_d)==-1) break;
					if(K_d<0) break;
					EEP_WriteFloat(PARAM_POS_P, K_p);
					EEP_ReadFloat(PARAM_POS_P, &K_p);
					EEP_WriteFloat(PARAM_POS_I, K_i);
					EEP_ReadFloat(PARAM_POS_I, &K_i);
					EEP_WriteFloat(PARAM_POS_D, K_d);
					EEP_ReadFloat(PARAM_POS_D, &K_d);
					USART_printf("PID parameters have been set to %f,%f,%f\r\n",3, K_p, K_i, K_d);
					Machine::getInstance().setMachinePID(K_p, K_i, K_d);
					break;
				}
				case 2: {
					EEP_ReadFloat(PARAM_ANG_P, &K_p);
					EEP_ReadFloat(PARAM_ANG_I, &K_i);
					EEP_ReadFloat(PARAM_ANG_D, &K_d);
					USART_printf("K_p:%f, K_i:%f, K_d:%f\r\n",3, K_p, K_i, K_d);
					USART_Puts("Change PID parameters for Machine Angle\r\nK_p: ");
					if(USART_scanf("%f",1, &K_p)==-1) break;
					if(K_p<0) break;
					USART_Puts("K_i: ");
					if(USART_scanf("%f",1, &K_i)==-1) break;
					if(K_i<0) break;
					USART_Puts("K_d: ");
					if(USART_scanf("%f",1, &K_d)==-1) break;
					if(K_d<0) break;
					EEP_WriteFloat(PARAM_ANG_P, K_p);
					EEP_ReadFloat(PARAM_ANG_P, &K_p);
					EEP_WriteFloat(PARAM_ANG_I, K_i);
					EEP_ReadFloat(PARAM_ANG_I, &K_i);
					EEP_WriteFloat(PARAM_ANG_D, K_d);
					EEP_ReadFloat(PARAM_ANG_D, &K_d);
					USART_printf("PID parameters have been set to %f,%f,%f\r\n",3,K_p,K_i,K_d);
					Machine::getInstance().setAnglePID(K_p, K_i, K_d);
					break;
				}
				case 3:{
					EEP_ReadFloat(PARAM_RANG_P, &K_p);
					EEP_ReadFloat(PARAM_RANG_D, &K_d);
					USART_printf("K_p:%f, K_d:%f\r\n",2, K_p, K_d);
					USART_Puts("Change PD parameters for Machine Target Angle\r\nK_p: ");
					if(USART_scanf("%f",1, &K_p)==-1) break;
					if(K_p<0) break;
					USART_Puts("K_d: ");
					if(USART_scanf("%f",1, &K_d)==-1) break;
					if(K_d<0) break;
					EEP_WriteFloat(PARAM_RANG_P, K_p);
					EEP_ReadFloat(PARAM_RANG_P, &K_p);
					EEP_WriteFloat(PARAM_RANG_D, K_d);
					EEP_ReadFloat(PARAM_RANG_D, &K_d);
					USART_printf("PD parameters have been set to %f,%f\r\n",2,K_p,K_d);
					Machine::getInstance().setRAnglePD(K_p, K_d);
					break;
				}
				case 4:{
					uint16_t offset;
					EEP_Read(PARAM_MOTOR_OFFSET, &offset);
					USART_printf("Offset:%d\r\n",1, offset);
					USART_Puts("Change the offset of Motor PWM\r\nOffset: ");
					if(USART_scanf("%d",1,&offset)==-1) break;
					if(offset<0) break;
					EEP_Write(PARAM_MOTOR_OFFSET, offset);
					EEP_Read(PARAM_MOTOR_OFFSET, &offset);
					USART_printf("PWM offset have been set to %d\r\n",1, offset);
					Machine::getInstance().setMotorPWMOffset(offset&0xFF);
					break;
				}
				default:
					break;
			}
		}
		break;
		case 'g': {
			int gx=Machine::getInstance().getGX();
			int gy=Machine::getInstance().getGY();
			int gz=Machine::getInstance().getGZ();
			long gz_i=Machine::getInstance().getGZI();
			USART_printf("Gyr:%d,%d,%d GZI:%d\r\n",4,gx,gy,gz,gz_i);
			break;
		}
		case 'a': {
			int ax=Machine::getInstance().getAX();
			int ay=Machine::getInstance().getAY();
			int az=Machine::getInstance().getAZ();
			USART_printf("Acc:%d,%d,%d\r\n",3,ax,ay,az);
			break;
		}
		case 'x': {
			long x=Machine::getInstance().getX();
			USART_printf("X: %d\r\n",1,x);
			break;
		}
		case 'y': {
			long y=Machine::getInstance().getY();
			USART_printf("Y: %d\r\n",1,y);
			break;
		}
		case 'p': {
			long a=Machine::getInstance().getAngle();
			long ea=Machine::getInstance().getEA();
			long x=Machine::getInstance().getX();
			long ex=Machine::getInstance().getEX();
			long y=Machine::getInstance().getY();
			long ey=Machine::getInstance().getEY();
			long a_i=Machine::getInstance().getAI();
			long v_l=Machine::getInstance().getVL();
			long v_r=Machine::getInstance().getVR();
			USART_printf("%d,%d,%d,%d,%d,%d,%d,%d,%d\r\n",9,a,ea,x,ex,y,ey,a_i,v_l,v_r);
			break;
		}
		case 'l': {
			uint8_t cmd;
			USART_Puts("View Log\r\n");
			USART_Puts("(0)x (1)y (2)angle (3)All (4)Exit: ");
			if (USART_scanf("%u",1,&cmd)==-1) break;
			if (cmd >= 5) {
				USART_Puts("Abort.\r\n");
				break;
			}
			switch (cmd) {
				case 0: {
					Machine::getInstance().printXLog();
					break;
				}
				case 1: {
					Machine::getInstance().printYLog();
					break;
				}
				case 2: {
					Machine::getInstance().printAngleLog();
					break;
				}
				case 3: {
					Machine::getInstance().printAllLog();
					break;
				}
			}
			break;
		}
		case '1': {
			Button_U1_Event();
			break;
		}
		case '2': {
			Button_U2_Event();
			break;
		}
		case 'd': {
			Machine::getInstance().deactivate();
			break;
		}
		case 'e': {
			Machine::getInstance().activate();
			break;
		}
		case 'n': {
			Maze maze=mouse->getMaze();
			Graph graph(maze);
			uint16_t index;

			USART_Puts("View Node\r\n");
			if (USART_scanf("%u",1,&index)==-1) break;
			if (index > 255) {
				USART_Puts("Abort.\r\n");
				break;
			}
			index&=0xFF;
			Node node=graph.getNodeByIndex(index);
			USART_printf("index:%d, done:%d, cost:%d, from:%d\nedges:\r\n",4,
					index,node.isDone(),node.getCost(),node.getFrom());
			Edges edges=node.getEdges();
			for(Edges::const_iterator it=edges.begin();it!=edges.end();it++){
				USART_printf("cost:%d, to:%d\r\n",2,(*it).cost,(*it).to);;
			}
		}
		case 'm': {
			Maze maze=mouse->getMaze();
			MazeData md=maze.getMazeData();
			for(int i=0;i<16;i++){
				for(int j=0;j<16;j++){
					uint8_t d=md[i*16+j].wall.half;
					char c=(d>9)?('a'+d):('0'+d);
					USART_Putc(c);
				}
				USART_Putc('\r');
				USART_Putc('\n');
			}
		}
	}
}
