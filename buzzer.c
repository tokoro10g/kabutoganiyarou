/*
 * buzzer.c
 *
 *  Created on: 2013/03/03
 *      Author: yuichi
 */

#include "buzzer.h"
#include "usart.h"

#define BUZZER_NBUF 128

typedef struct {
	uint16_t freq;
	uint16_t duration;
} Note;

static Note NoteBuffer[BUZZER_NBUF];

static struct {
	unsigned playing :1;
	unsigned enabled :1;
	unsigned readPos :7;
	unsigned writePos :7;
} BuzzerStatus;

void Buzzer_Config() {
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM13, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM11, ENABLE);

	GPIO_InitTypeDef GPIO_InitStruct;
	/* Buzzer */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_6;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource6, GPIO_AF_TIM13 );

	NVIC_InitTypeDef NVIC_InitStruct;
	NVIC_InitStruct.NVIC_IRQChannel = TIM1_TRG_COM_TIM11_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 3;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);

	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStruct;
	TIM_TimeBaseInitStruct.TIM_Period = 999;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStruct.TIM_Prescaler = (uint16_t) ((SystemCoreClock / 2)
			/ 60000) - 1; //60kHz
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM13, &TIM_TimeBaseInitStruct);
	TIM_OCInitTypeDef TIM_OCInitStruct;
	TIM_OCInitStruct.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStruct.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStruct.TIM_Pulse = 1;
	TIM_OCInitStruct.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OC1Init(TIM13, &TIM_OCInitStruct);
	TIM_ARRPreloadConfig(TIM13, ENABLE);
	TIM_Cmd(TIM13, ENABLE);

	TIM_TimeBaseInitStruct.TIM_Period = 1000;
	TIM_TimeBaseInitStruct.TIM_ClockDivision = 0;
	TIM_TimeBaseInitStruct.TIM_Prescaler = (uint16_t) ((SystemCoreClock)
			/ 1000000) - 1; // 100kHz
	TIM_TimeBaseInitStruct.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInit(TIM11, &TIM_TimeBaseInitStruct);
	TIM_ITConfig(TIM11, TIM_IT_Update, ENABLE);
	TIM_Cmd(TIM11, ENABLE);

	Buzzer_SetStatus(1);
	Buzzer_Add(0, 0);
}

void Buzzer_SetStatus(uint8_t newstat) {
	BuzzerStatus.enabled = newstat;
}

void Buzzer_Add(uint16_t freq, uint16_t duration) {
	if (!BuzzerStatus.enabled)
		return;
	Note note = { freq, duration };
	NoteBuffer[BuzzerStatus.writePos] = note;
	BuzzerStatus.writePos++;
	BuzzerStatus.writePos&=BUZZER_NBUF-1;
}

void Buzzer_SetFreq(uint16_t freq) {
	if (!BuzzerStatus.enabled)
		return;
	TIM13 ->ARR = 60000 / freq - 1;
	TIM13 ->CCR1 = (TIM13 ->ARR + 1) >> 2;
	TIM_SetCounter(TIM13, 0);
}

void Buzzer_Stop() {
	TIM13 ->CCR1 = 0;
	BuzzerStatus.playing = 0;
}

void Buzzer_Play() {
	BuzzerStatus.readPos = 0;
	BuzzerStatus.playing = 1;
}

void Buzzer_PlayEnchukei(){
		Buzzer_Add(1047, 400);
		Buzzer_Add(1397, 380);
		Buzzer_Add(0, 20);
		Buzzer_Add(1397, 200);
		Buzzer_Add(1568, 200);
		Buzzer_Add(1760, 400);
		Buzzer_Add(1568, 200);
		Buzzer_Add(1397, 200);
		Buzzer_Add(1760, 800);
		Buzzer_Add(0, 400);
		Buzzer_Add(1047, 180);
		Buzzer_Add(0, 20);
		Buzzer_Add(1047, 180);
		Buzzer_Add(0, 20);
		Buzzer_Add(1397, 600);
		Buzzer_Add(2093, 200);
		Buzzer_Add(1760, 400);
		Buzzer_Add(1568, 200);
		Buzzer_Add(1397, 200);
		Buzzer_Add(1568, 800);
		Buzzer_Add(0, 400);
		Buzzer_Add(1568, 200);
		Buzzer_Add(1760, 200);
		Buzzer_Add(1865, 380);
		Buzzer_Add(0, 20);
		Buzzer_Add(1865, 380);
		Buzzer_Add(0, 20);
		Buzzer_Add(1865, 400);
		Buzzer_Add(1760, 200);
		Buzzer_Add(1568, 200);
		Buzzer_Add(1760, 200);
		Buzzer_Add(1568, 200);
		Buzzer_Add(1397, 600);
		Buzzer_Add(0, 200);
		Buzzer_Add(1397, 200);
		Buzzer_Add(1760, 200);
		Buzzer_Add(1568, 180);
		Buzzer_Add(0, 20);
		Buzzer_Add(1568, 400);
		Buzzer_Add(1047, 200);
		Buzzer_Add(1760, 400);
		Buzzer_Add(1568, 400);
		Buzzer_Add(1397, 800);
}

void Buzzer_Event() {
	if (!BuzzerStatus.playing)
		return;
	if (NoteBuffer[BuzzerStatus.readPos].duration == 0) {
		BuzzerStatus.readPos++;
		BuzzerStatus.readPos&=BUZZER_NBUF-1;
		TIM13 ->CCR1 = 0;
		if (NoteBuffer[BuzzerStatus.readPos].freq
				&& NoteBuffer[BuzzerStatus.readPos].duration)
			Buzzer_SetFreq(NoteBuffer[BuzzerStatus.readPos].freq);
	} else {
		NoteBuffer[BuzzerStatus.readPos].duration--;
	}
}
