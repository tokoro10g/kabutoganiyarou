/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_it.h"
#include "stm32f4xx_conf.h"

#include "adc.h"
#include "usart.h"
#include "button.h"
#include "buzzer.h"
#include "hdsp2000lp.h"
#include "service.h"
#include "machine.h"
#include "i2c.h"

extern uint8_t USART_RxBuffer[32][64];
extern uint8_t USART_RxBufPos;
extern uint8_t USART_RxBufLinePos;

extern uint8_t USART_TxBuffer[32][128];
extern uint8_t USART_TxReadLinePos;
extern uint8_t USART_TxBufLinePos;

static struct {
	unsigned SW_U1_OState :1;
	unsigned SW_U1_EdgeFlag :1;
	unsigned SW_U2_OState :1;
	unsigned SW_U2_EdgeFlag :1;
} ButtonState;

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

void WWDG_IRQHandler(void) {
	USART_Puts("WWDG\r\n");
}

/**
 * @brief   This function handles NMI exception.
 * @param  None
 * @retval None
 */
void NMI_Handler(void) {
	/* This interrupt is generated when HSE clock fails */

	if (RCC_GetITStatus(RCC_IT_CSS ) != RESET) {
		/* At this stage: HSE, PLL are disabled (but no change on PLL config) and HSI
		 is selected as system clock source */

		/* Enable HSE */
		RCC_HSEConfig(RCC_HSE_ON );

		/* Enable HSE Ready and PLL Ready interrupts */
		RCC_ITConfig(RCC_IT_HSERDY | RCC_IT_PLLRDY, ENABLE);

		/* Clear Clock Security System interrupt pending bit */
		RCC_ClearITPendingBit(RCC_IT_CSS );

		/* Once HSE clock recover, the HSERDY interrupt is generated and in the RCC ISR
		 routine the system clock will be reconfigured to its previous state (before
		 HSE clock failure) */
	}
}

/**
 * @brief  This function handles Hard Fault exception.
 * @param  None
 * @retval None
 */
void HardFault_Handler(void) {
	USART_Puts("HardFault\n");
	/* Go to infinite loop when Hard Fault exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles Memory Manage exception.
 * @param  None
 * @retval None
 */
void MemManage_Handler(void) {
	/* Go to infinite loop when Memory Manage exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles Bus Fault exception.
 * @param  None
 * @retval None
 */
void BusFault_Handler(void) {
	/* Go to infinite loop when Bus Fault exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles Usage Fault exception.
 * @param  None
 * @retval None
 */
void UsageFault_Handler(void) {
	/* Go to infinite loop when Usage Fault exception occurs */
	while (1) {
	}
}

/**
 * @brief  This function handles SVCall exception.
 * @param  None
 * @retval None
 */
void SVC_Handler(void) {
}

/**
 * @brief  This function handles Debug Monitor exception.
 * @param  None
 * @retval None
 */
void DebugMon_Handler(void) {
}

/**
 * @brief  This function handles PendSVC exception.
 * @param  None
 * @retval None
 */
void PendSV_Handler(void) {
}

/**
 * @brief  This function handles SysTick Handler.
 * @param  None
 * @retval None
 */
void SysTick_Handler(void) {
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/

/**
 * @brief  This function handles PPP interrupt request.
 * @param  None
 * @retval None
 */
/*void PPP_IRQHandler(void)
 {
 }*/

/**
 * @brief  This function handles RCC interrupt request.
 * @param  None
 * @retval None
 */
void RCC_IRQHandler(void) {
	if (RCC_GetITStatus(RCC_IT_HSERDY ) != RESET) {
		/* Clear HSERDY interrupt pending bit */
		RCC_ClearITPendingBit(RCC_IT_HSERDY );

		/* Check if the HSE clock is still available */
		if (RCC_GetFlagStatus(RCC_FLAG_HSERDY ) != RESET) {
			/* Enable PLL: once the PLL is ready the PLLRDY interrupt is generated */
			RCC_PLLCmd(ENABLE);
		}
	}

	if (RCC_GetITStatus(RCC_IT_PLLRDY ) != RESET) {
		/* Clear PLLRDY interrupt pending bit */
		RCC_ClearITPendingBit(RCC_IT_PLLRDY );

		/* Check if the PLL is still locked */
		if (RCC_GetFlagStatus(RCC_FLAG_PLLRDY ) != RESET) {
			/* Select PLL as system clock source */
			RCC_SYSCLKConfig(RCC_SYSCLKSource_PLLCLK );
		}
	}
}

void TIM8_TRG_COM_TIM14_IRQHandler(void) {
	if (TIM_GetITStatus(TIM14, TIM_IT_Update ) != RESET) {
		if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_0 ) == SET) {
			if (ButtonState.SW_U1_OState == 0) {
				ButtonState.SW_U1_OState = 1;
				if (ButtonState.SW_U1_EdgeFlag == 0) {
					ButtonState.SW_U1_EdgeFlag = 1;
				}
			} else if (ButtonState.SW_U1_EdgeFlag) {
				Button_U1_Event();
				ButtonState.SW_U1_EdgeFlag = 0;
			}
		} else {
			ButtonState.SW_U1_OState = 0;
			ButtonState.SW_U1_EdgeFlag = 0;
		}

		if (GPIO_ReadInputDataBit(GPIOC, GPIO_Pin_1 ) == SET) {
			if (ButtonState.SW_U2_OState == 0) {
				ButtonState.SW_U2_OState = 1;
				if (ButtonState.SW_U2_EdgeFlag == 0) {
					ButtonState.SW_U2_EdgeFlag = 1;
				}
			} else if (ButtonState.SW_U2_EdgeFlag) {
				Button_U2_Event();
				ButtonState.SW_U2_EdgeFlag = 0;
			}
		} else {
			ButtonState.SW_U2_OState = 0;
			ButtonState.SW_U2_EdgeFlag = 0;
		}
		TIM_ClearITPendingBit(TIM14, TIM_IT_Update );
	}
}

void TIM1_TRG_COM_TIM11_IRQHandler(void) {
	if (TIM_GetITStatus(TIM11, TIM_IT_Update ) != RESET) {
		Buzzer_Event();
		TIM_ClearITPendingBit(TIM11, TIM_IT_Update );
	}
}

void TIM8_BRK_TIM12_IRQHandler(void) {
	if (TIM_GetITStatus(TIM12, TIM_IT_Update ) != RESET) {
		/*
		 uint32_t sum = 0;
		 for (int i = 0; i < 4; i++) {
		 ADC_RegularChannelConfig(ADC1, ADC_Channel_2, 1, ADC_SampleTime_56Cycles);
		 ADC_SoftwareStartConv(ADC1);
		 while (ADC_GetFlagStatus(ADC1, ADC_FLAG_EOC) == RESET)
		 ;
		 sum += ADC_GetConversionValue(ADC1);
		 }
		 */
		//uint16_t data = sum >> 2;
		/*
		 if (data > 100) {
		 USART_WriteHalfWordByDigit(data);
		 USART_WriteNewLine();
		 }
		 */
		TIM_ClearITPendingBit(TIM12, TIM_IT_Update );
		MachineReadSensors();
	}
}

void TIM1_UP_TIM10_IRQHandler() {
	if (TIM_GetITStatus(TIM10, TIM_IT_Update ) != RESET) {
		TIM_ClearITPendingBit(TIM10, TIM_IT_Update );
		HDSP_Refresh();
	}
}

void TIM1_BRK_TIM9_IRQHandler() {
	if (TIM_GetITStatus(TIM9, TIM_IT_Update ) != RESET) {
		TIM_ClearITPendingBit(TIM9, TIM_IT_Update );
		ServiceHandler();
	}
}

void TIM5_IRQHandler(){
	static int toggle=0;
	if (TIM_GetITStatus(TIM5, TIM_IT_Update ) != RESET) {
		TIM_ClearITPendingBit(TIM5, TIM_IT_Update );
		if(ADC_Status){
			toggle=!toggle;
			if(toggle){
				GPIO_SetBits(GPIOB,GPIO_Pin_14|GPIO_Pin_15);
				ADC_SoftwareStartConv(ADC1);
			} else {
				GPIO_ResetBits(GPIOB,GPIO_Pin_14|GPIO_Pin_15);
			}
		}
	}
}

void I2C2_EV_IRQHandler() {

}

void I2C2_ER_IRQHandler() {
	/* Check on I2C2 SMBALERT flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_SMBALERT )) {
		USART_Puts("[err]I2C_SMBALERT\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_SMBALERT );
	}
	/* Check on I2C2 Time out flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_TIMEOUT )) {
		USART_Puts("[err]I2C_TIMEOUT\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_TIMEOUT );
	}
	/* Check on I2C2 Arbitration Lost flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_ARLO )) {
		USART_Puts("[err]I2C_ARBITRATION_LOST\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_ARLO );
	}
	/* Check on I2C2 PEC error flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_PECERR )) {
		USART_Puts("[err]I2C_PECERR\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_PECERR );
	}
	/* Check on I2C2 Overrun/Underrun error flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_OVR )) {
		USART_Puts("[err]I2C_OVERRUN\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_OVR );
	}
	/* Check on I2C2 Acknowledge failure error flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_AF )) {
		USART_Puts("[err]I2C_ACKFAILED\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_AF );
	}
	/* Check on I2C2 Bus error flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_BERR )) {
		USART_Puts("[err]I2C_BUSERR\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_BERR );
	}
	/* Check on I2C2 error flag and clear it */
	if (I2C_GetITStatus(I2C2, I2C_IT_ERR ) != RESET) {
		USART_Puts("[err]I2C_ERR\r\n");
		I2C_ClearITPendingBit(I2C2, I2C_IT_ERR );
	}
	I2C_ErrFlag=1;
}

void USART3_IRQHandler(void) {
	if (USART_GetITStatus(USART3, USART_IT_RXNE ) != RESET) {
		uint8_t ch = USART_ReceiveData(USART3 ) & 0xFF;
		if (ch == 0x08) {
			if (USART_RxBufPos == 0)
				return;
			USART_RxBuffer[USART_RxBufLinePos][--USART_RxBufPos] = 0;
			USART_Puts("\b \b");
			return;
		} else if (ch == 0x7f) {
			return;
		}
		USART_RxBuffer[USART_RxBufLinePos][USART_RxBufPos++] = ch;
		USART_RxBufPos &= 0x3F;
		USART_Putc(ch);
		if (ch == '\r') {
			USART_Putc('\n');
			USART_RxBufLinePos++;
			USART_RxBufLinePos &= 31;
			USART_RxBufPos = 0;
		}
		USART_ClearITPendingBit(USART3, USART_IT_RXNE );
	}
}

void DMA1_Stream3_IRQHandler(void) {
	DMA1->LIFCR|=DMA_LIFCR_CFEIF3;
	if (DMA1->LISR|DMA_LISR_TCIF3) {
	//if(DMA_GetITStatus(DMA1_Stream3,DMA_IT_TCIF4)){
		DMA_ClearITPendingBit(DMA1_Stream3,DMA_IT_HTIF4|DMA_IT_TCIF4);
		//DMA1->LIFCR|=DMA_LIFCR_CHTIF3|DMA_LIFCR_CTCIF3|DMA_LIFCR_CTEIF3|DMA_LIFCR_CFEIF3;
		USART_PushTxBuffer();
	}
	DMA_ClearITPendingBit(DMA1_Stream3,DMA_IT_HTIF4|DMA_IT_TCIF4|DMA_IT_FEIF4);
}
