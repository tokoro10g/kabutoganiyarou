#include <algorithm>
#include "viewer.hpp"
#include "usart.h"


const char* Viewer::DIRCHR=" ^> v   <";

Viewer::Viewer(){
	maze=new Maze(0);
	clear_coord();
}

Viewer::Viewer(const Maze& _maze){
	maze=new Maze(_maze);
	clear_coord();
}

Viewer::Viewer(const Maze& _maze,const Route& _route){
	maze=new Maze(_maze);
	append(_route);
	clear_coord();
}

Viewer::Viewer(const Maze& _maze,const Route& _route,const Coord& _c){
	maze=new Maze(_maze);
	append(_route);
	append(_c);
}

void Viewer::append(const Maze& _maze){
	delete maze;
	maze=new Maze(_maze);
}

void Viewer::append(const Route& _route){
	route=_route;
}

void Viewer::append(const Coord& _c){
	c.x=_c.x;
	c.y=_c.y;
	c.dir.half=_c.dir.half;
}

void Viewer::clear_maze(){
	delete maze;
	maze=new Maze(0);
}

void Viewer::clear_route(){
	route.clear();
}

void Viewer::clear_coord(){
	c.x=-1;
	c.y=-1;
	c.dir.half=0;
}

void Viewer::show() const{
	int cursorx=0,cursory=0;
	int w=maze->getWidth();
	bool routeflag=(route.size()>0);

	VCP_Putc('_');
	for(int i=0;i<w;i++){
		VCP_Putc('_');
		VCP_Putc('_');
	}
	VCP_Putc('\r');
	VCP_Putc('\n');

	MazeData md=maze->getMazeData();
	for(MazeData::const_iterator it=md.begin();it!=md.end();it++){
		int index=cursorx+w*cursory;
		if(cursorx==0)
			VCP_Putc('|');

		if((*it).wall.bits.SOUTH) {
			VCP_Puts("\x1b[4m");	// underline
		}
		if(cursorx==c.x&&cursory==c.y){
			VCP_Putc(DIRCHR[c.dir.half]);
		} else if(routeflag&&index==(*(route.begin()))){
			VCP_Putc('S');
		} else if(routeflag&&index==(*(route.end()-1))){
			VCP_Putc('E');
		} else if(std::find(route.begin(),route.end(),index)!=route.end()){
			VCP_Putc('*');
		} else {
			VCP_Putc(' ');
		}
		VCP_Puts("\x1b[0m");

		if((*it).wall.bits.EAST){
			VCP_Putc('|');
		} else if((*it).wall.bits.SOUTH) {
			VCP_Putc('_');
		} else {
			VCP_Putc(' ');
		}

		cursorx++;
		if(cursorx==w){
			cursorx=0;
			cursory++;
			VCP_Putc('\r');
			VCP_Putc('\n');
		}
	}
}
